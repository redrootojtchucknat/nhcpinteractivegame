package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class ProfileScreen implements Screen {

	final InteractiveGame game;
	final int i;
	
	protected String presidentName;
	protected float polaroidX2;
	protected float polaroidY2;
	protected int sampleSnapshotOffset;
	
	protected Stage stage;
	protected Table table;
	protected Table scrollTable;
	protected FileHandle biographyFile;
	
	protected Image background;
	protected Image backToTimeline;
	protected Image photoGallery;
	protected Image relevantEvents;
	protected Image speeches;
	protected Image portrait;
	
	protected ArrayList<Snapshot> sampleSnapshots;
	protected ArrayList<Image> snapshots;
	protected ArrayList<Image> polaroids;
	
	protected Label name;
	protected Label biography;
	protected ScrollPane biographyScrollPane;
	
	protected FileHandle audioFile;
	protected Music audioBiography;
	
	public ProfileScreen( final InteractiveGame gam, String presidentName )
	{
		game = gam;
		this.presidentName = presidentName;
		i = game.presidentNames.indexOf( presidentName );
		stage = new Stage();
		polaroidY2 = 492;
        polaroidX2 = 662;
        sampleSnapshotOffset = 220;
		
		backToTimeline = new Image();
		photoGallery = new Image();
		relevantEvents = new Image();
		speeches = new Image();
		background = new Image( game.assets.manager.get( game.assets.profileBG ) );
		
		audioFile = Gdx.files.internal( "profile/audioBiography/" + presidentName + ".mp3" );
		if( audioFile.exists() )
		{
			audioBiography = Gdx.audio.newMusic( audioFile );
		}
		
		sampleSnapshots = new ArrayList<Snapshot>();
		snapshots = new ArrayList<Image>();
        polaroids = new ArrayList<Image>();
		
		name = new Label( null, game.skin, "pencilPete" );
		name.setText( presidentName );
		name.setAlignment( Align.center );
		name.setRotation(0.5f);
		name.setPosition(game.windowWidth * .6625f, game.windowHeight * .8375f );
		
		// define the scroll pane of the profile screen
		biography = new Label(null, game.skin, "sylfaenSmall" );
		biography.setWrap(true);
		biographyScrollPane = new ScrollPane(biography, game.skin);
		biographyScrollPane.setScrollingDisabled(true, false);
		biographyScrollPane.setOverscroll(false, true);
		biographyScrollPane.setupFadeScrollBars(0, 0);
		biographyScrollPane.setRotation(0.5f);
		biographyScrollPane.setBounds(game.windowWidth * .4125f, game.windowHeight * .4f, game.windowWidth * .525f, game.windowHeight * .4f);
		
		photoGallery.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y ) {
				game.buttonSoundEffect.play();
				game.setScreen( game.photoGalleryScreens.get( i ) );
			}
		});
		
		speeches.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y ) {
				game.buttonSoundEffect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.speechScreensEn.get( i ) );
				else game.setScreen( game.speechScreensFi.get( i ) );
			}
		});
		
		relevantEvents.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y ) {
				game.buttonSoundEffect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.relevantEventsScreensEn.get( i ) );
				else game.setScreen( game.relevantEventsScreensFi.get( i ) );
			}
		});
		
		portrait = new Image( game.assets.manager.get(game.assets.profilePortraits.get( i ) ) );
		portrait.setSize( game.windowWidth * .328125f, game.windowHeight * .731444f );
		portrait.setPosition( game.windowWidth * .05f, game.windowHeight * .125f );
		
		sampleSnapshots = game.utility.loadFourSnapshots(presidentName);
		int j = 0;
		final String pName = presidentName;
		for(Snapshot sample: sampleSnapshots){
        	snapshots.add( new Image( sample.getSnapshot() ) );
        	polaroids.add( new Image( game.assets.manager.get(game.assets.polaroid700x700) ) );
        	final Texture image = sample.getSnapshot();
        	final String text = sample.getCaption();
        	polaroids.get( j ).addListener( new ClickListener() {
    			public void clicked( InputEvent event, float x, float y ) {
    				game.shutterClickSoundEffect.play();
    				game.setScreen( new SnapshotPopupScreen( game, pName, image, text) );
    			}
    		});
        	j++;
        }
		
		int k = 0;
		float x = game.windowWidth * .546875f;
		// position the snapshots
		for( k=0;k<snapshots.size();k++ ){
			polaroids.get(k).setWidth(polaroids.get(k).getWidth() / 3.5f);
        	polaroids.get(k).setHeight(polaroids.get(k).getHeight() / 3.5f);
            float tempY2 = polaroidY2 / 3.5f;
            float tempX2 = polaroidX2 / 3.5f;
            snapshots.get(k).setWidth( tempX2 );	// scale to fit screen
            snapshots.get(k).setHeight( tempY2 );	// scale to fit screen
            polaroids.get(k).setX( x );
            snapshots.get(k).setX( x + (15/4) );
            polaroids.get(k).setY(game.windowHeight / 5);
    		snapshots.get(k).setY(polaroids.get(k).getY() + 163/3.5f);
            x += sampleSnapshotOffset;
		}
		
		table = new Table();
		table.setFillParent( true );
		stage.setViewport( game.viewport );
		stage.addActor( background );
		for( k = 0; k < snapshots.size(); k++ )
		{
			stage.addActor( snapshots.get( k ) );
			stage.addActor( polaroids.get( k ) );
		}
		stage.addActor( portrait );
		stage.addActor( name );
		stage.addActor( biographyScrollPane );
		stage.addActor( table );
		table.row().expand();
		table.add( photoGallery ).center().bottom().padBottom(game.padding * 11).padRight(game.padding * 11 ).colspan(3);
		table.row().bottom().padBottom(game.padding).expandX();
		table.add( backToTimeline ).left().padLeft( game.padding * 2f ).padBottom( game.padding * 2f);
		table.add( relevantEvents ).center();
		table.add( speeches ).right().padRight(game.padding);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		biographyScrollPane.setScrollY( 0 );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
		relevantEvents.addAction(Actions.sequence( Actions.moveBy( 0, -relevantEvents.getHeight() ), Actions.moveBy(0, relevantEvents.getHeight(), 0.5f)));
		speeches.addAction(Actions.sequence( Actions.moveBy( 0, -speeches.getHeight() ), Actions.moveBy(0, speeches.getHeight(), 0.5f)));
		for(int k = 0; k<snapshots.size();k++){
        	snapshots.get(k).addAction( Actions.sequence( Actions.moveBy( game.windowWidth - snapshots.get(k).getX(), 0 ), Actions.moveBy( -game.windowWidth + snapshots.get(k).getX(), 0, 0.5f)) );
        	polaroids.get(k).addAction(Actions.sequence( Actions.moveBy( game.windowWidth - polaroids.get(k).getX(), 0 ), Actions.moveBy( -game.windowWidth + polaroids.get(k).getX(), 0, 0.5f)) );
        }
		if( audioFile.exists() )
		{
			game.backgroundMusic.setVolume( .2f );
			audioBiography.play();
		}
	}

	@Override
	public void hide() {
		if( audioFile.exists() )
		{
			game.backgroundMusic.setVolume( 1f );
			audioBiography.setLooping( true );
			audioBiography.stop();
		}
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
