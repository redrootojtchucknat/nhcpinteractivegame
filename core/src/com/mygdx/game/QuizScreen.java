package com.mygdx.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class QuizScreen implements Screen{
	final InteractiveGame game;
	protected Stage stage;
	protected Table table;
	protected int score;
	
	protected Label scoreLabel;
	protected Label correctLabel;
	protected ArrayList<Question> questionItems;
	protected ArrayList<Label> questions;
	protected ArrayList<Label> choice1s;
	protected ArrayList<Label> choice2s;
	protected ArrayList<Label> choice3s;
	protected ArrayList<Integer> correctAnswers;
	protected ArrayList<Group> questionSets;
	protected ArrayList<Group> groupsA;
	protected ArrayList<Group> groupsB;
	protected ArrayList<Group> groupsC;
	protected ArrayList<Label> questionNos;
	protected ArrayList<ClickListener> clickListeners;
	protected Image aButton;
	protected Image bButton;
	protected Image cButton;
	protected Image backToTimeline;
	protected Image background;
	
	public QuizScreen(final InteractiveGame gam){
		//initialize all class variables here
		game = gam;
		final QuizScreen screen = this;
		stage = new Stage();
		table = new Table();
		score = 0;
		int i;
		
		background = new Image( game.assets.manager.get(game.assets.chalkboardBG) );
		questionItems = new ArrayList<Question>();
		questions = new ArrayList<Label>();
		choice1s = new ArrayList<Label>();
		choice2s = new ArrayList<Label>();
		choice3s = new ArrayList<Label>();
		correctAnswers = new ArrayList<Integer>();
		questionSets = new ArrayList<Group>();
		groupsA = new ArrayList<Group>();
		groupsB = new ArrayList<Group>();
		groupsC = new ArrayList<Group>();
		questionNos = new ArrayList<Label>();
		clickListeners = new ArrayList<ClickListener>();
		
		questionItems = game.utility.loadAllQuestions(game.language);
		
		// randomise questions
		long seed = System.nanoTime();
		Collections.shuffle( questionItems, new Random( seed ) );
		
		backToTimeline = new Image( game.assets.manager.get(game.assets.backToTimelineEn) );
		aButton = new Image( game.assets.manager.get(game.assets.aButton) );
		bButton = new Image( game.assets.manager.get(game.assets.bButton) );
		cButton = new Image( game.assets.manager.get(game.assets.cButton) );
		
		aButton.setPosition( game.windowWidth * .2f, game.windowHeight * .34f );
		bButton.setPosition( game.windowWidth * .2f, game.windowHeight * .215f );
		cButton.setPosition( game.windowWidth * .2f, game.windowHeight * .09f );
		
		i = 0;
		for(final Question questionItem : questionItems){
			Label questionNo = new Label( null, game.skin, "chandaFelizBigChanda" );
			Label question = new Label(null, game.skin, "eraserBigWhite");
			Label choice1 = new Label(null, game.skin, "sylfaenMedium");
			Label choice2 = new Label(null, game.skin, "sylfaenMedium");
			Label choice3 = new Label(null, game.skin, "sylfaenMedium");
			Group questionSet = new Group();
			Group groupA = new Group();
			Group groupB = new Group();
			Group groupC = new Group();
			
			questionNo.setPosition( game.windowWidth * .5f, game.windowHeight * .85f );
			questionNo.setAlignment( Align.center );
			questionNo.setText( "Question # " + ( ( Integer )(i + 1) ).toString() );
			
			question.setBounds( game.windowWidth * .125f, game.windowHeight * .5f, game.windowWidth * .75f, game.windowHeight * .3f);
			question.setWrap( true );
			question.setAlignment( Align.center );
			question.setText(questionItem.getQuestion());
			
			Image whiteBoxA = new Image( game.assets.manager.get(game.assets.whiteBox) );
			Image whiteBoxB = new Image( game.assets.manager.get(game.assets.whiteBox) );
			Image whiteBoxC = new Image( game.assets.manager.get(game.assets.whiteBox) );
			
			whiteBoxA.setPosition( game.windowWidth * .25f, game.windowHeight * .34f );
			whiteBoxB.setPosition( game.windowWidth * .25f, game.windowHeight * .215f );
			whiteBoxC.setPosition( game.windowWidth * .25f, game.windowHeight * .09f );
			
			// change the font size depending on length of text
			if( questionItem.getChoice1().length() > 26 )
			{
				choice1.setStyle( new LabelStyle( game.skin.get( "sylfaenSmall", LabelStyle.class ) ) );
			}
			if( questionItem.getChoice2().length() > 26 )
			{
				choice2.setStyle( new LabelStyle( game.skin.get( "sylfaenSmall", LabelStyle.class ) ) );
			}
			if( questionItem.getChoice3().length() > 26 )
			{
				choice3.setStyle( new LabelStyle( game.skin.get( "sylfaenSmall", LabelStyle.class ) ) );
			}
			
			choice1.setBounds( whiteBoxA.getX(), whiteBoxA.getY(), whiteBoxA.getWidth(), whiteBoxA.getHeight() );
			choice1.setAlignment( Align.center );
			choice1.setWrap( true );
			choice1.setText(questionItem.getChoice1());
			
			choice2.setBounds( whiteBoxB.getX(), whiteBoxB.getY(), whiteBoxB.getWidth(), whiteBoxB.getHeight() );
			choice2.setAlignment( Align.center );
			choice2.setWrap( true );
			choice2.setText(questionItem.getChoice2());
			
			choice3.setBounds( whiteBoxC.getX(), whiteBoxC.getY(), whiteBoxC.getWidth(), whiteBoxC.getHeight() );
			choice3.setAlignment( Align.center );
			choice3.setWrap( true );
			choice3.setText(questionItem.getChoice3());
			
			correctAnswers.add( new Integer( questionItem.getCorrectAnswer() ) );
			
			groupA.addActor( whiteBoxA );
			groupB.addActor( whiteBoxB );
			groupC.addActor( whiteBoxC );
			groupA.addActor( choice1 );
			groupB.addActor( choice2 );
			groupC.addActor( choice3 );
			
			questionSet.addActor( questionNo );
			questionSet.addActor( question );
			questionSet.addActor( groupA );
			questionSet.addActor( groupB );
			questionSet.addActor( groupC );
			if( questionItem.getImagePath() != null )
			{
				Image questionImage = new Image( new Texture( Gdx.files.internal( "quiz/questionPictures/" + questionItem.getImagePath() ) ) );
				questionImage.setPosition( game.windowWidth * .65f, game.windowHeight * .09f );
				float ratio;
				if(questionImage.getWidth()>=500){
					if(questionImage.getWidth()>=questionImage.getHeight()){
						ratio = 500/questionImage.getWidth();
						questionImage.setWidth(500);
						questionImage.setHeight(ratio * questionImage.getHeight());
					}
					else{
						ratio = 500/questionImage.getHeight();
						questionImage.setHeight(500);
						questionImage.setWidth(ratio * questionImage.getWidth());
					}
					
				}
				else if(questionImage.getHeight()>=500){
					if(questionImage.getWidth()>=questionImage.getHeight()){
						ratio = 500/questionImage.getWidth();
						questionImage.setWidth(500);
						questionImage.setHeight(ratio * questionImage.getHeight());
					}
					else{
						ratio = 500/questionImage.getHeight();
						questionImage.setHeight(500);
						questionImage.setWidth(ratio * questionImage.getWidth());
					}
				}
				questionImage.setPosition(questionImage.getX() + (500/2 - questionImage.getWidth()/2), questionImage.getY() + (500/2 - questionImage.getHeight()/2));
				questionSet.addActor( questionImage );
			}
			questionSets.add( questionSet );
			groupsA.add( groupA );
			groupsB.add( groupB );
			groupsC.add( groupC );
			questionNos.add( questionNo );
			if( i != 0 )
			{
				questionSet.setVisible( false );
			}
			i++;
		}
		
		
		i = 0;
		for( Group groupA: groupsA )
		{
			final int j = i;
			groupA.addListener( new ClickListener() {
				public void clicked( InputEvent event, float x, float y ){
					if( correctAnswers.get( j ) == 1 )
					{
						game.correctSoundEffect.play();
						goToNextQuestion( j );
					}
					else
					{
						game.wrongSoundEffect.play();
						game.setScreen( new WrongAnswerScreen( game, score ) );
						screen.dispose();
					}
				}
			});
			i++;
		}
		
		i = 0;
		for( Group groupB: groupsB )
		{
			final int j = i;
			groupB.addListener( new ClickListener() {
				public void clicked( InputEvent event, float x, float y ){
					if( correctAnswers.get( j ) == 2 )
					{
						game.correctSoundEffect.play();
						goToNextQuestion( j );
					}
					else
					{
						game.wrongSoundEffect.play();
						game.setScreen( new WrongAnswerScreen( game, score ) );
						screen.dispose();
					}
				}
			});
			i++;
		}
		
		i = 0;
		for( Group groupC: groupsC )
		{
			final int j = i;
			groupC.addListener( new ClickListener() {
				public void clicked( InputEvent event, float x, float y ){
					if( correctAnswers.get( j ) == 3 )
					{
						game.correctSoundEffect.play();
						goToNextQuestion( j );	
					}
					else
					{
						game.wrongSoundEffect.play();
						game.setScreen( new WrongAnswerScreen( game, score ) );
						screen.dispose();
					}
				}
			});
			i++;
		}
		
		backToTimeline.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.closeBackSoundEFfect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.timelineScreenEn );
				else game.setScreen( game.timelineScreenFi );
			}
		});
		
		correctLabel = new Label( null, game.skin, "eraserBigWhite" );
		correctLabel.setPosition( game.windowWidth * .5f, game.windowHeight * .5f );
		correctLabel.setAlignment( Align.center );
		correctLabel.setText( "Correct!" );
		correctLabel.setVisible( false );
		
		table.setFillParent( true );
		stage.setViewport( game.viewport );
		stage.addActor(background);;
		for( i = 0; i < questionItems.size(); i++ )
		{
			stage.addActor( questionSets.get( i ) );
		}
		stage.addActor( aButton );
		stage.addActor( bButton );
		stage.addActor( cButton );
		stage.addActor( table );
		stage.addActor( correctLabel );
		
		table.add( backToTimeline ).bottom().left().expand().padBottom( game.padding * 2f ).padLeft( game.padding * 2f );
	}
	
	// this function makes the screen go to the next question
	public void goToNextQuestion( final int j )
	{
		correctLabel.addAction( Actions.sequence( Actions.visible( true ), Actions.fadeOut( 1.5f ), Actions.visible( false ), Actions.fadeIn( .1f ) ) );
		questionSets.get( j ).setVisible( false );
		groupsA.get( j ).setVisible( false );
		groupsB.get( j ).setVisible( false );
		groupsC.get( j ).setVisible( false );
		updateScore( score + 1 );
		if( j < questionSets.size() - 1 )
		{
			questionSets.get( j + 1 ).setVisible( true );
			groupsA.get( j + 1 ).setVisible( true );
			groupsB.get( j + 1 ).setVisible( true );
			groupsC.get( j + 1 ).setVisible( true );
		}
		else
		{
			game.setScreen( game.quizCompletedScreen );
		}
	}
	
	// make everything but the first question visible
	public void resetQuestions()
	{
		int i;
		for( i = 0; i < questionSets.size(); i++ )
		{
			questionNos.get( i ).setText( "Question # " + ( ( Integer )(i + 1) ).toString() );
			if( i == 0 )
			{
				questionSets.get( i ).setVisible( true );
				groupsA.get( i ).setVisible( true );
				groupsB.get( i ).setVisible( true );
				groupsC.get( i ).setVisible( true );
			}
			else
			{
				questionSets.get( i ).setVisible( false );
				groupsA.get( i ).setVisible( false );
				groupsB.get( i ).setVisible( false );
				groupsC.get( i ).setVisible( false );
			}
		}
	}
	
	// update the score label properly
	public void updateScore( int score )
	{
		this.score = score;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.camera.update();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		updateScore( 0 );
		resetQuestions();
		Gdx.input.setInputProcessor( stage );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		/*
		stage.dispose();
		backgroundTexture.dispose();
		*/
	}

}
