package com.mygdx.game;

// class holding the necessary information of the question class
public class Question {
	private String question;
	private String choice1;
	private String choice2;
	private String choice3;
	private int correctAnswer;
	private String imagePath;
	
	public Question( String question, String choice1, String choice2, String choice3, int correctAnswer, String imagePath )
	{
		this.setQuestion(question);
		this.setChoice1(choice1);
		this.setChoice2(choice2);
		this.setChoice3(choice3);
		this.setCorrectAnswer(correctAnswer);
		this.setImagePath( imagePath );
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the choice1
	 */
	public String getChoice1() {
		return choice1;
	}

	/**
	 * @param choice1 the choice1 to set
	 */
	public void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	/**
	 * @return the choice2
	 */
	public String getChoice2() {
		return choice2;
	}

	/**
	 * @param choice2 the choice2 to set
	 */
	public void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	/**
	 * @return the choice3
	 */
	public String getChoice3() {
		return choice3;
	}

	/**
	 * @param choice3 the choice3 to set
	 */
	public void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	/**
	 * @return the correctAnswer
	 */
	public int getCorrectAnswer() {
		return correctAnswer;
	}

	/**
	 * @param correctAnswer the correctAnswer to set
	 */
	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
