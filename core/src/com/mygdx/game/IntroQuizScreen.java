package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

// screen before going to the quiz
public class IntroQuizScreen implements Screen {
	
	final InteractiveGame game;
	
	private Stage stage;
	private Table table;
	
	private TextureRegion introQuizBGRegion;
	
	private Image backToTimeline;
	private Image getStarted;
	
	public IntroQuizScreen( final InteractiveGame gam )
	{
		game = gam;
		
		stage = new Stage();
		introQuizBGRegion = new TextureRegion();
		backToTimeline = new Image();
		getStarted = new Image();
		table = new Table();
		
		// add listeners for the buttons
		backToTimeline.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.closeBackSoundEFfect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.timelineScreenEn );
				else game.setScreen( game.timelineScreenFi );
			}
		});		
		
		getStarted.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.buttonSoundEffect.play();
				game.setScreen( new QuizScreen( game ) );
			}
		});
		
		table.setFillParent( true );
		stage.setViewport( game.viewport );
		stage.addActor( table );
		table.row().bottom().expandY();
		table.add( getStarted ).padBottom( game.padding * 3f );
		table.row().expandX().padBottom( game.padding * 2f );
		table.add( backToTimeline ).left().padLeft( game.padding * 2f);
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);		
		
		game.batch.setProjectionMatrix(game.camera.combined);
		// draw the header and timeline separate from the stage
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw(introQuizBGRegion, 0, 0);
		game.batch.enableBlending();
		game.batch.end();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );

	}

	@Override
	public void show() {
		if(game.language.equals("en")){
			introQuizBGRegion.setRegion( game.assets.manager.get(game.assets.introQuizBGEn) );
			
			// define the "back to timeline" button
	        backToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.backToTimelineEn ) ) ) );
	        
	        // define the "get started" button
	        getStarted.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.getStartedEn ) ) ) );
		}
		else if(game.language.equals("fi")){
			introQuizBGRegion.setRegion( game.assets.manager.get(game.assets.introQuizBGFi ) );
			
			// define the "back to timeline" button
	        backToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.backToTimelineFi ) ) ) );
	        
	        // define the "get started" button
	        getStarted.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.getStartedFi ) ) ) );
		}
		Gdx.input.setInputProcessor( stage );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
