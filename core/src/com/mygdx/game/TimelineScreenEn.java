package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class TimelineScreenEn extends TimelineScreen {

	public TimelineScreenEn(InteractiveGame gam) {
		super(gam);
		int i, parity;
        
        // call the English header
        headerTexture = game.assets.manager.get(game.assets.timelineHeaderEn);
        
        // define the "change language" button
        changeLanguage.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.changeLanguageEn) ) ) );
        
        // define the "take the quiz" button
        takeTheQuiz.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.takeTheQuizEn) ) ) );
        
        // define and add elements to the ArrayLists
        for( String president: game.presidentNames )
        {
        	i = game.presidentNames.indexOf(president);
        	presidentTerms.add( new Image( game.assets.manager.get(game.assets.presidentTermsEn.get( i ) ) ) );
        }
        
        // set the Y and X coordinates of the Image actors
        i = 370;
        parity = 1;
        for( Image presidentTerm: presidentTerms )
        {
    		presidentTerm.setX( i );
    		// portrait up
        	if( parity % 2 != 0 )
        		presidentTerm.setY(game.windowHeight / 2 + game.windowHeight / 8);
        	// portrait down
        	else
        		presidentTerm.setY(game.windowHeight / 6);
        	i += portraitOffset;
        	parity++;
        }
        
        // add the image for the term of service of the presidents
        for( Image presidentTerm: presidentTerms )
        	presidentGroup.addActor( presidentTerm );
	}

}
