package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class RelevantEventsScreen implements Screen {
	final InteractiveGame game;
	
	protected int selector;
	protected float maxPicWidth;
	protected float maxPicHeight;
	protected Stage stage;
	protected Table table;
	
	private static Texture backgroundTexture;
	
	protected TextureRegionDrawable closeTextureRegionDrawable;
	
	protected Label eventHeader;
	protected ArrayList<Event> events;
	protected ArrayList<Label> eventTitles;
	protected ArrayList<Label> eventContents;
	protected ArrayList<Image> eventPictures;
	protected ArrayList<ScrollPane> eventScrollPanes;
	protected Image prev;
	protected Image next;
	protected Image close;
	
	public RelevantEventsScreen( final InteractiveGame gam, String presidentName )
	{
		game = gam;
		maxPicWidth = game.windowWidth * .241f;
		maxPicHeight = game.windowHeight * .593f;
		final int i = game.presidentNames.indexOf( presidentName );
		int j;
		selector = 0;
		
		stage = new Stage();
		events = new ArrayList<Event>();
		eventTitles = new ArrayList<Label>();
		eventPictures = new ArrayList<Image>();
		eventContents = new ArrayList<Label>();
		eventScrollPanes = new ArrayList<ScrollPane>();
		table = new Table();
		
		backgroundTexture = game.assets.manager.get(game.assets.relevantEventsBG);
		
		// close button
		close = new Image();
		
		//close.setPosition( game.windowWidth * .75f, game.windowHeight * .9f );
		
		// define the english and filipino headers
		eventHeader = new Label(null, game.skin, "sylfaenMedium");
		eventHeader.setPosition( game.windowWidth * .225f, game.windowHeight * .85f );
		
		// define positions for arrows as well as the arrows themselves
		prev = new Image( game.assets.manager.get(game.assets.prevBrown) );
		next = new Image( game.assets.manager.get(game.assets.nextBrown) );
		prev.setPosition( game.windowWidth * .125f, game.windowHeight * .35f );
		next.setPosition( game.windowWidth * .825f, game.windowHeight * .35f );
		prev.setVisible( false );

		close.addListener( new ClickListener() {
			final int j = i;
			public void clicked( InputEvent event, float x, float y )
			{
				game.closeBackSoundEFfect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.profileScreensEn.get( j ) );
				else game.setScreen( game.profileScreensFi.get( j ) );
			}
		});
		
		prev.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				if( selector > 0 )
				{
					game.pageFlipSoundEffect.play();
					hideCurrentEvent( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
					selector--;
					resetEventScrollPane( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
					if( selector == 0 )
						prev.setVisible( false );
					next.setVisible( true );
				}
			}
		});
		
		next.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				if( selector < eventScrollPanes.size() - 1 )
				{
					game.pageFlipSoundEffect.play();
					hideCurrentEvent( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
					selector++;
					resetEventScrollPane( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
					if( selector == eventScrollPanes.size() - 1 )
						next.setVisible( false );
					prev.setVisible( true );
				}
			}
		});
		
		events = game.utility.loadAllEvents( presidentName );
		// define speech content and its scroll pane
		for( j = 0; j < events.size(); j++ )
		{
			Label tempEventTitle = new Label( null, game.skin, "twCenBig" );
			Image tempEventImage = new Image( new Texture( Gdx.files.internal( "relevantEvents/eventPictures/" + presidentName + "/" + events.get( j ).getPath() ) ) );
			Label tempEventContent = new Label( null, game.skin, "sylfaenSmall" );
			FileHandle contentFile = Gdx.files.internal( "relevantEvents/content/" + presidentName + "/" + events.get( j ).getContent() );
			if( events.get( j ).getTitle().length() > 60 )
			{
				tempEventTitle.setStyle( new LabelStyle( game.skin.get( "twCenSmall", LabelStyle.class ) ) );
			}
			else if( events.get( j ).getTitle().length() > 30 )
			{
				tempEventTitle.setStyle( new LabelStyle( game.skin.get( "twCenMedium", LabelStyle.class ) ) );
			}

			tempEventContent.setWrap( true );
			tempEventTitle.setPosition( game.windowWidth * .225f, game.windowHeight * .725f );
			tempEventTitle.setWrap(true);
			tempEventTitle.setWidth( game.windowWidth * .521f );
			tempEventImage.setPosition( game.windowWidth * .575f, game.windowHeight * .05f );
			tempEventContent.setAlignment( Align.right );
			tempEventTitle.setText( events.get( j ).getTitle() );
			tempEventContent.setText( contentFile.readString() );
			System.out.println( tempEventImage.getWidth() );
			if( tempEventImage.getWidth() == 1 )
			{
				tempEventImage.setName( "default" );
				tempEventContent.setAlignment( Align.center );
			}
			else
			{
				tempEventImage.setName( "other" );
				
				float ratio;
				if( tempEventImage.getWidth() <= maxPicWidth && tempEventImage.getHeight() <= maxPicHeight )
				{
					if( tempEventImage.getWidth() / tempEventImage.getHeight() > .725f )
					{
						ratio = maxPicWidth / tempEventImage.getWidth();
						tempEventImage.setWidth( maxPicWidth );
						tempEventImage.setHeight( ratio * tempEventImage.getHeight() );
					}
					else
					{
						ratio = maxPicHeight / tempEventImage.getHeight();
						tempEventImage.setWidth(  ratio * tempEventImage.getWidth() );
						tempEventImage.setHeight( maxPicHeight );
					}
				}
				else if(tempEventImage.getWidth()>=maxPicWidth){
					if(tempEventImage.getWidth()>=tempEventImage.getHeight()){
						ratio = maxPicWidth/tempEventImage.getWidth();
						tempEventImage.setWidth(maxPicWidth);
						tempEventImage.setHeight(ratio * tempEventImage.getHeight());
					}
					else{
						ratio = maxPicHeight/tempEventImage.getHeight();
						tempEventImage.setHeight(maxPicHeight);
						tempEventImage.setWidth(ratio * tempEventImage.getWidth());
					}
					
				}
				else if(tempEventImage.getHeight()>=maxPicHeight){
					if(tempEventImage.getWidth()>=tempEventImage.getHeight()){
						ratio = maxPicWidth/tempEventImage.getWidth();
						tempEventImage.setWidth(maxPicWidth);
						tempEventImage.setHeight(ratio * tempEventImage.getHeight());
					}
					else{
						ratio = maxPicHeight/tempEventImage.getHeight();
						tempEventImage.setHeight(maxPicHeight);
						tempEventImage.setWidth(ratio * tempEventImage.getWidth());
					}
				}
			}
			tempEventImage.setPosition(tempEventImage.getX() + (maxPicWidth/2 - tempEventImage.getWidth()/2), tempEventImage.getY() + (maxPicHeight/2 - tempEventImage.getHeight()/2));
			
			eventTitles.add( tempEventTitle );
			eventPictures.add( tempEventImage );
			eventContents.add( tempEventContent );
			
			ScrollPane eventScrollPane = new ScrollPane(eventContents.get( j ), game.skin);
			eventScrollPane.setScrollingDisabled(true, false);
			eventScrollPane.setOverscroll(false, true);
			eventScrollPane.setupFadeScrollBars(0, 0);
			if( j != 0 )
			{
				tempEventTitle.setVisible( false );
				tempEventImage.setVisible( false );
				eventScrollPane.setVisible( false );
			}
			eventScrollPanes.add( eventScrollPane );
		}
		
		if( events.size() == 1 ) next.setVisible( false );
		
		for( j = 0; j < events.size(); j++ )
		{
			stage.addActor( eventTitles.get( j ) );
			stage.addActor( eventPictures.get( j ) );
			stage.addActor( eventScrollPanes.get( j ) );
		}
			
		stage.setViewport( game.viewport );
		stage.addActor( close );
		stage.addActor( eventHeader );
		stage.addActor( prev );
		stage.addActor( next );
	}
	
	// set the position of the scrollpane
	public void resetEventScrollPane( ScrollPane esp, Label title, Image image )
	{
		esp.setRotation( 0.25f );
		esp.setScrollY( 0 );
		if( image.getName().equals( "other" ) ) esp.setBounds(game.windowWidth * .1875f, game.windowHeight * .05f, game.windowWidth * .375f, game.windowHeight * .6f);
		else esp.setBounds(game.windowWidth * .225f, game.windowHeight * .05f, game.windowWidth * .55f, game.windowHeight * .6f);
		esp.setVisible( true );
		title.setVisible( true );
		image.setVisible( true );
	}
	
	public void hideCurrentEvent( ScrollPane esp, Label title, Image image )
	{
		esp.setVisible( false );
		title.setVisible( false );
		image.setVisible( false );
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw( backgroundTexture, 0, 0 );
		game.batch.enableBlending();
		game.batch.end();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor( stage );
		hideCurrentEvent( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
		selector = 0;
		resetEventScrollPane( eventScrollPanes.get( selector ), eventTitles.get( selector ), eventPictures.get( selector ) );
		prev.setVisible( false );
		next.setVisible( true );
		if( events.size() == 1 )
			next.setVisible( false );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
		backgroundTexture.dispose();
	}

}
