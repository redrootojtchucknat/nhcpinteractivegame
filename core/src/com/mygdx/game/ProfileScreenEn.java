package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ProfileScreenEn extends ProfileScreen {
	public ProfileScreenEn( InteractiveGame gam, String presidentName)
	{
		super( gam, presidentName );
		
		biography.setText( game.assets.biographiesEn.get( super.i ).readString() );
		
		// load the images
		backToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.backToTimelineEn) ) ) );
		photoGallery.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.photoGalleryTextEn) ) ) );
		relevantEvents.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.relevantEventsEn) ) ) );
		speeches.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.speechesEn) ) ) );

		backToTimeline.addListener( new ClickListener() {
        	public void clicked( InputEvent event, float x, float y ) {
        		game.closeBackSoundEFfect.play();
        		game.setScreen( game.timelineScreenEn );
        	}
        });
	}
}
