package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

public class Snapshot {
	private Texture snapshot;
	private String caption;
	
	
	public Snapshot(Texture snapshot, String caption) {
		this.snapshot = snapshot;
		this.caption = caption;
	}
	
	public Texture getSnapshot() {
		return snapshot;
	}
	public void setSnapshot(Texture snapshot) {
		this.snapshot = snapshot;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
}
