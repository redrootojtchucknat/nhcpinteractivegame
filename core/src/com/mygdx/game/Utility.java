package com.mygdx.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.badlogic.gdx.graphics.Texture;

public class Utility{
	//VARIABLES FOR RETRIEVING ALL IMAGES IN A FOLDER
    // File representing the folder that you select using a FileChooser

    // array of supported extensions (use a List if you prefer)
    final String[] EXTENSIONS = new String[]{
        "gif", "png", "bmp", "jpg" // and other formats you need
    };
    // filter to identify images based on their extensions
    final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };
    //END
    
    // function to read all biographies
    public void readBiography(String presidentName){
    	try {
			String file = "assets\\biography\\" + presidentName + ".txt";
    		FileReader fr = new FileReader(file);
		    BufferedReader br = new BufferedReader(fr); 
		    Stream<String> lines = br.lines();
		    Iterator<String> iter = lines.iterator();
		    while (iter.hasNext())
		         System.out.println(iter.next());
		    br.close();
		 }
    	catch(Exception e){
    		System.out.println("Error reading biographies");
    	}
    }
    
    // function to load 4 pictures to display at the president's profile page
    public ArrayList<Snapshot> loadFourSnapshots(String presidentName){
    	ArrayList<Snapshot> tempsnapshots = new ArrayList<Snapshot>();
    	int i = 0;
    	try
        {
            Element root = new XmlReader().parse( Gdx.files.internal("photoGallery/snapshots/" + presidentName + "/snapshotlist.xml" ) );
            for (XmlReader.Element snapshot : root.getChildrenByName("snapshot")) {
            	tempsnapshots.add( new Snapshot( new Texture( Gdx.files.internal("photoGallery/snapshots/" + presidentName + "/" + snapshot.get( "image", "" ))), snapshot.get( "caption", "" ) ) );
            	if( i == 3 )
            		break;
            	i++;
            } 
            
        }
    	catch(Exception e)
    	{
            System.out.println( "Error loading snapshots of " + presidentName );
            e.printStackTrace();
        }
		return tempsnapshots;
    }
    
    // function to load all the pictures of the presidents as well as the corresponding captions
    public ArrayList<Snapshot> loadAllSnapshots(String presidentName){
		ArrayList<Snapshot> tempsnapshots = new ArrayList<Snapshot>();
		try
        {
            Element root = new XmlReader().parse( Gdx.files.internal("photoGallery/snapshots/" + presidentName + "/snapshotlist.xml" ) );
            for (XmlReader.Element snapshot : root.getChildrenByName("snapshot")) {
            	tempsnapshots.add( new Snapshot( new Texture( Gdx.files.internal("photoGallery/snapshots/" + presidentName + "/" + snapshot.get( "image", "" ))), snapshot.get( "caption", "" ) ) );
            } 
            
        }
    	catch(Exception e)
    	{
            System.out.println( "Error when reading snapshotlist" );
            e.printStackTrace();
        }
		return tempsnapshots;		
	}

    // function to load all the speeches of the president passed as an argument
	public ArrayList<Speech> loadAllSpeeches( String presidentName )
	{
    	ArrayList<Speech> speeches = new ArrayList<Speech>();
    	try
        {
    		Element root = new XmlReader().parse( Gdx.files.internal( "speech/speeches/" + presidentName + "/speechlist.xml" ) );
            
    		for (XmlReader.Element speech : root.getChildrenByName("speech")) {
            	speeches.add( new Speech( speech.get( "title", "" ), speech.get( "date", "" ), speech.get( "content", "" ), speech.get( "audio", null ) ) );
            } 
        }
    	catch(Exception e)
    	{
            System.out.println( "Error when reading speeches of " + presidentName );
            e.printStackTrace();
        }
		return speeches;
    }
	
	// function to load all the events of a certain language of a president
	public ArrayList<Event> loadAllEvents( String presidentName )
	{
		ArrayList<Event> events = new ArrayList<Event>();
		try
		{
			Element root = new XmlReader().parse( Gdx.files.internal( "relevantEvents/content/" + presidentName + "/eventlist.xml" ) );
			for (XmlReader.Element event : root.getChildrenByName("event")) {
            	events.add( new Event( event.get( "title", "" ), event.get( "content", "" ), event.get( "image", "default.png" ) ) );
            } 
		}
		catch( Exception e )
		{
			System.out.println( "Error when reading events of " + presidentName );
			e.printStackTrace();
		}
		return events;
	}
	
	// function to load all the questions of a certain language
	public ArrayList<Question> loadAllQuestions( String language )
	{
		ArrayList<Question> questions = new ArrayList<Question>();
		try
		{
			Element root = new XmlReader().parse( Gdx.files.internal( "quiz/" + language + "/questionlist.xml" ) );
			for (XmlReader.Element question : root.getChildrenByName("item")) {
            	int answer = Integer.parseInt(question.get("answer","0"));
				questions.add( new Question( question.get("question",""), question.get("choice1",""), question.get("choice2",""), question.get("choice3",""), answer, question.get("image", null) ) );
            } 
		}
		catch( Exception e )
		{
			System.out.println( "Error when reading questions" );
			e.printStackTrace();
		}
		return questions;
	}
    
	// function to load the presidents that will populate this instance of the game
    public ArrayList<String> getPresidentNames(){
    	ArrayList<String> presidentNames = new ArrayList<String>();
    	try {
    		FileHandle file = Gdx.files.internal( "presidentNames.txt" );
		    Reader reader = file.reader();
		    BufferedReader br = new BufferedReader( reader );
		    String line;
		    while( ( line = br.readLine() ) != null )
		    {
		    	presidentNames.add( line );
		    }
		    /*
		    Stream<String> lines = br.lines();
		    Iterator<String> iter = lines.iterator();
		    while (iter.hasNext())
		    	presidentNames.add(iter.next());
		    	*/
		    br.close();
		    
		 }
    	catch(Exception e){
    		System.out.println("Error loading the presidentNames list");
    		e.printStackTrace();
    	}
    	
    	return presidentNames;
    }
}