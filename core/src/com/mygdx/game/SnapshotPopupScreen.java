package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class SnapshotPopupScreen implements Screen{
	final InteractiveGame game;
	final String presidentName;
	float maxWidth = 800;
	float maxHeight = 600;
	protected String text;
	
	protected Texture snapshotTexture;
	
	protected Image snapshot;
	protected Image overlay;
	
	protected FileHandle captionFile;
	protected Label caption;
	protected ScrollPane captionScrollPane;
	
	protected Stage stage;
	protected Table scrollPaneTable;
	
	public SnapshotPopupScreen(final InteractiveGame gam, final String presidentName, Texture image, String text) {
		game = gam;
		this.presidentName = presidentName;
		this.text = text;
		final int i = game.presidentNames.indexOf(presidentName);
		snapshotTexture = image;
		
		snapshot = new Image( snapshotTexture );
		overlay = new Image( game.assets.manager.get(game.assets.overlayTexture) );
		
		if( !text.equals("")){
			captionFile = Gdx.files.internal( "photoGallery/snapshots/" + presidentName + "/captions/" + text);
			if(captionFile.exists()){
				caption = new Label(null, game.skin, "sylfaenSmall" );
				caption.setWidth( game.windowWidth * .5f );
				caption.setWrap(true);
				caption.setText( captionFile.readString() );
				captionScrollPane = new ScrollPane(caption, game.skin, "scrollBG");
				captionScrollPane.setScrollingDisabled(true, false);
				captionScrollPane.setOverscroll(false, true);
				captionScrollPane.setFadeScrollBars( false );
				
				// set a maximum height for the caption (so as not to look ugly for small images)
				float spHeight = ( game.windowHeight - snapshot.getHeight() ) / 2 + game.windowHeight / 10 - game.windowHeight * .05f;
				if( spHeight > game.windowHeight * .25f ) spHeight = game.windowHeight * .25f;
				captionScrollPane.setBounds(game.windowWidth * .1f, game.windowHeight * 0.05f, game.windowWidth * .8f, spHeight);
			}
		}
		
		Color overlayColor = overlay.getColor();
		overlayColor.a = 0.1f;
		
		float ratio;
		if(snapshot.getWidth()>=maxWidth){
			if(snapshot.getWidth()>=snapshot.getHeight()){
				ratio = maxWidth/snapshot.getWidth();
				snapshot.setWidth(maxWidth);
				snapshot.setHeight(ratio * snapshot.getHeight());
			}
			else{
				ratio = maxHeight/snapshot.getHeight();
				snapshot.setHeight(maxHeight);
				snapshot.setWidth(ratio * snapshot.getWidth());
			}
			
		}
		else if(snapshot.getHeight()>=maxHeight){
			if(snapshot.getWidth()>=snapshot.getHeight()){
				ratio = maxWidth/snapshot.getWidth();
				snapshot.setWidth(maxWidth);
				snapshot.setHeight(ratio * snapshot.getHeight());
			}
			else{
				ratio = maxHeight/snapshot.getHeight();
				snapshot.setHeight(maxHeight);
				snapshot.setWidth(ratio * snapshot.getWidth());
			}
		}
		else{
			if(snapshot.getWidth()>=snapshot.getHeight()){
				ratio = 600/snapshot.getWidth();
				snapshot.setWidth(600);
				snapshot.setHeight(ratio * snapshot.getHeight());
			}
			else{
				ratio = 450/snapshot.getHeight();
				snapshot.setHeight(450);
				snapshot.setWidth(ratio * snapshot.getWidth());
			}
		}
		
		snapshot.setX((game.windowWidth - snapshot.getWidth())/2);
		snapshot.setY((game.windowHeight - snapshot.getHeight())/2 + game.windowHeight/10);
		
		overlay.addListener(new ClickListener(){
			public void clicked( InputEvent event, float x, float y ) {
    			game.setScreen( game.photoGalleryScreens.get(i) );
    		}
		});
		
		stage = new Stage();
		stage.setViewport( game.viewport );
		stage.addActor(overlay);
		if(!text.equals("")){
			stage.addActor( captionScrollPane );
		}
		stage.addActor(snapshot);
		
	}

	@Override
	public void render(float delta) {
		
		game.batch.enableBlending();
		game.camera.update();
		game.batch.setProjectionMatrix(game.camera.combined);
		stage.act();
		stage.draw();
				
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor( stage );
		if(!text.equals("")){
			captionScrollPane.setScrollY( 0 );
		}
		
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
		snapshotTexture.dispose();
	}
	
	
}
