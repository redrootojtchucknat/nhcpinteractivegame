package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class SpeechScreenEn extends SpeechScreen {

	public SpeechScreenEn(InteractiveGame gam, String presidentName) {
		super(gam, presidentName);
		
		closeTextureRegionDrawable = new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.closeEn) ) );
		close.setDrawable( closeTextureRegionDrawable );
		close.setSize(closeTextureRegionDrawable.getRegion().getRegionWidth(), closeTextureRegionDrawable.getRegion().getRegionHeight());
		close.setPosition( game.windowWidth - close.getWidth() - game.padding * 2f, game.windowHeight - close.getHeight() - game.padding * 2f );
		speechHeader.setText( "Speech" );
	}
}
