package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;


// this class stores all assets so we don't have to load them more than is necessary
public class Assets {
	final InteractiveGame game;
	public final AssetManager manager = new AssetManager(); 
	
	public final AssetDescriptor<Texture> chalkboardBG;
	public final AssetDescriptor<Texture> chalkboardBGFlip;
	public final AssetDescriptor<Texture> backToTimelineEn;
	public final AssetDescriptor<Texture> backToTimelineFi;
	public final AssetDescriptor<Texture> polaroid700x700;
	public final AssetDescriptor<Texture> nextBlack;
	public final AssetDescriptor<Texture> prevBlack;
	public final AssetDescriptor<Texture> closeEn;
	public final AssetDescriptor<Texture> closeFi;
	
	// assets local to timeline
	public final AssetDescriptor<Texture> nextWhite;
	public final AssetDescriptor<Texture> prevWhite;
	public final AssetDescriptor<Texture> timelineHeaderEn;
	public final AssetDescriptor<Texture> takeTheQuizEn;
	public final AssetDescriptor<Texture> changeLanguageEn;
	public final AssetDescriptor<Texture> timelineHeaderFi;
	public final AssetDescriptor<Texture> takeTheQuizFi;
	public final AssetDescriptor<Texture> changeLanguageFi;

	public final ArrayList<AssetDescriptor<Texture>> presidentPortraits;
	public final ArrayList<AssetDescriptor<Texture>> presidentNames;
	public final ArrayList<AssetDescriptor<Texture>> presidentTermsEn;
	public final ArrayList<AssetDescriptor<Texture>> presidentTermsFi;
	
	// assets local to profile
	public final AssetDescriptor<Texture> profileBG;
	public final AssetDescriptor<Texture> photoGalleryTextEn;
	public final AssetDescriptor<Texture> relevantEventsEn;
	public final AssetDescriptor<Texture> speechesEn;
	public final AssetDescriptor<Texture> photoGalleryTextFi;
	public final AssetDescriptor<Texture> relevantEventsFi;
	public final AssetDescriptor<Texture> speechesFi;
	public final ArrayList<AssetDescriptor<Texture>> profilePortraits;
	public final ArrayList<FileHandle> biographiesEn;
	public final ArrayList<FileHandle> biographiesFi;
	
	// assets local to photo gallery
	public final AssetDescriptor<Texture> pgHeaderEn;
	public final AssetDescriptor<Texture> pgHeaderFi;
	public final AssetDescriptor<Texture> backToProfileEn;
	public final AssetDescriptor<Texture> backToProfileFi;
	public final AssetDescriptor<Texture> pgBackground;
	public final AssetDescriptor<Texture> pgBackgroundWrap;
	public final AssetDescriptor<Texture> frameTop;
	public final AssetDescriptor<Texture> frameLeft;
	public final AssetDescriptor<Texture> frameRight;
	public final AssetDescriptor<Texture> frameBottom;
	
	// assets local to relevant events
	public final AssetDescriptor<Texture> relevantEventsBG;
	public final AssetDescriptor<Texture> nextBrown;
	public final AssetDescriptor<Texture> prevBrown;
	
	// assets local to speeches
	public final AssetDescriptor<Texture> speechBG;
	public final ArrayList<AssetDescriptor<Texture>> snapshots;
	
	// assets local to quiz
	public final AssetDescriptor<Texture> aButton;
	public final AssetDescriptor<Texture> bButton;
	public final AssetDescriptor<Texture> cButton;
	public final AssetDescriptor<Texture> whiteBox;
	
	//assets local to snapshot pop up
	public final AssetDescriptor<Texture> overlayTexture;
	public final AssetDescriptor<Texture> scrollBG;
	
	//assets locat to introquiz
	public final AssetDescriptor<Texture> getStartedEn;
	public final AssetDescriptor<Texture> introQuizBGEn;
	public final AssetDescriptor<Texture> getStartedFi;
	public final AssetDescriptor<Texture> introQuizBGFi;
	
	//assets local to wrongAnswer
	public final AssetDescriptor<Texture> wrongBGEn;
	public final AssetDescriptor<Texture> wrongBGFi;
	public final AssetDescriptor<Texture> whiteBackToTimelineEn;
	public final AssetDescriptor<Texture> whiteBackToTimelineFi;
	public final AssetDescriptor<Texture> wrongTakeQuizAgainEn;
	public final AssetDescriptor<Texture> wrongTakeQuizAgainFi;
	
	//assets local to quizCompleted
	public final AssetDescriptor<Texture> quizCompleteBGEn;
	public final AssetDescriptor<Texture> quizCompleteBGFi;
	public final AssetDescriptor<Texture> quizCompleteBackToTimelineEn;
	public final AssetDescriptor<Texture> quizCompleteBackToTimelineFi;
	
	public Assets( final InteractiveGame gam )
	{
		game = gam;
		
		chalkboardBG = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/background.png" ), Texture.class );
		chalkboardBGFlip = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/backgroundFlip.png" ), Texture.class );
		backToTimelineEn = new AssetDescriptor<Texture>( Gdx.files.internal( "backToTimelineEn.png" ), Texture.class );
		backToTimelineFi = new AssetDescriptor<Texture>( Gdx.files.internal( "backToTimelineFi.png" ), Texture.class );
		polaroid700x700 = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/polaroid2.png" ), Texture.class );
		nextBlack = new AssetDescriptor<Texture>( Gdx.files.internal( "speech/next.png" ), Texture.class );
		prevBlack = new AssetDescriptor<Texture>( Gdx.files.internal( "speech/prev.png" ), Texture.class );
		closeEn = new AssetDescriptor<Texture>( Gdx.files.internal( "closeEn.png" ), Texture.class );
		closeFi = new AssetDescriptor<Texture>( Gdx.files.internal( "closeFi.png" ), Texture.class );
		
		// assets local to timeline
		nextWhite = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/next.png" ), Texture.class );
		prevWhite = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/prev.png" ), Texture.class );
		timelineHeaderEn = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/en/header.png" ), Texture.class );
		takeTheQuizEn = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/en/takeTheQuiz.png" ), Texture.class );
		changeLanguageEn = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/en/changeLanguage.png" ), Texture.class );
		timelineHeaderFi = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/fi/header.png" ), Texture.class );
		takeTheQuizFi = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/fi/takeTheQuiz.png" ), Texture.class );
		changeLanguageFi = new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/fi/changeLanguage.png" ), Texture.class );
		presidentPortraits = new ArrayList<AssetDescriptor<Texture>>();
		presidentNames = new ArrayList<AssetDescriptor<Texture>>();
		presidentTermsEn = new ArrayList<AssetDescriptor<Texture>>();
		presidentTermsFi = new ArrayList<AssetDescriptor<Texture>>();
		
		// assets local to profile
		profileBG = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/background.png" ), Texture.class );
		photoGalleryTextEn = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/en/photoGallery.png" ), Texture.class );
		relevantEventsEn = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/en/relevantEvents.png" ), Texture.class );
		speechesEn = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/en/speeches.png" ), Texture.class );
		photoGalleryTextFi = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/fi/photoGallery.png" ), Texture.class );
		relevantEventsFi = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/fi/relevantEvents.png" ), Texture.class );
		speechesFi = new AssetDescriptor<Texture>( Gdx.files.internal( "profile/fi/speeches.png" ), Texture.class );
		profilePortraits = new ArrayList<AssetDescriptor<Texture>>();
		biographiesEn = new ArrayList<FileHandle>();
		biographiesFi = new ArrayList<FileHandle>();
		
		// assets local to photo gallery
		pgHeaderEn = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/en/header.png" ), Texture.class );
		pgHeaderFi =  new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/fi/header.png" ), Texture.class );
        backToProfileEn = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/en/backToProfile.png" ), Texture.class );
        backToProfileFi =  new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/fi/backToProfile.png" ), Texture.class );
        pgBackground = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/background.png"), Texture.class );
        pgBackgroundWrap = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/background.png"), Texture.class );
        frameTop = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/frame_top.png"), Texture.class );
        frameLeft = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/frame_left.png"), Texture.class );
        frameRight = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/frame_right.png"), Texture.class );
        frameBottom = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/frame_bottom.png"), Texture.class );
		
		// assets local to relevant events
		relevantEventsBG = new AssetDescriptor<Texture>( Gdx.files.internal( "relevantEvents/background.png" ), Texture.class );
		nextBrown = new AssetDescriptor<Texture>( Gdx.files.internal( "relevantEvents/next.png" ), Texture.class );
		prevBrown = new AssetDescriptor<Texture>( Gdx.files.internal( "relevantEvents/prev.png" ), Texture.class );
		
		// assets local to speeches
		speechBG = new AssetDescriptor<Texture>( Gdx.files.internal( "speech/background.png" ), Texture.class );
		snapshots = new ArrayList<AssetDescriptor<Texture>>();
		
		// assets local to quiz
		aButton = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/a.png" ), Texture.class );
		bButton = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/b.png" ), Texture.class );
		cButton = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/c.png" ), Texture.class );
		whiteBox = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/whiteBox.png" ), Texture.class );
		
		//assets local to snapshot pop up
		overlayTexture = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/overlay2.png"), Texture.class );
		scrollBG = new AssetDescriptor<Texture>( Gdx.files.internal( "photoGallery/scrollBG.png" ), Texture.class );
		
		//assets locat to introquiz
		getStartedEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/getStarted.png" ), Texture.class );
		introQuizBGEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/background.png" ), Texture.class );
		getStartedFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/getStarted.png" ), Texture.class );
		introQuizBGFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/background.png" ), Texture.class );
		
		//assets local to wrongAnswer
		wrongBGEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/wrongBG.png" ), Texture.class );
		wrongBGFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/wrongBG.png" ), Texture.class );
		whiteBackToTimelineEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/whiteBackToTimeline.png" ), Texture.class );
		whiteBackToTimelineFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/whiteBackToTimeline.png" ), Texture.class );
		wrongTakeQuizAgainEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/wrongTakeQuizAgain.png" ), Texture.class );
		wrongTakeQuizAgainFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/wrongTakeQuizAgain.png" ), Texture.class );
		
		//assets local to quizCompleted
		quizCompleteBGEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/quizCompleteBG.png" ), Texture.class );
		quizCompleteBGFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/quizCompleteBG.png" ), Texture.class );
		quizCompleteBackToTimelineEn = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/en/quizCompleteBacToTimeline.png" ), Texture.class );
		quizCompleteBackToTimelineFi = new AssetDescriptor<Texture>( Gdx.files.internal( "quiz/fi/quizCompleteBacToTimeline.png" ), Texture.class );
		
		for( String president: game.presidentNames )
		{
			presidentPortraits.add( new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/portraits/" + president + ".png" ), Texture.class ) );
			presidentNames.add( new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/names/" + president + ".png" ), Texture.class ) );
			presidentTermsEn.add( new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/en/terms/" + president + ".png" ), Texture.class ) );
			presidentTermsFi.add( new AssetDescriptor<Texture>( Gdx.files.internal( "timeline/fi/terms/" + president + ".png" ), Texture.class ) );
			profilePortraits.add( new AssetDescriptor<Texture>( Gdx.files.internal( "profile/profilePicture/" + president + ".png" ), Texture.class ) );
			snapshots.add( new AssetDescriptor<Texture>( Gdx.files.internal( "speech/snapshot/" + president + ".png" ), Texture.class ) );
			biographiesEn.add( Gdx.files.internal( "profile/en/biography/" + president + ".txt" ) );
			biographiesFi.add( Gdx.files.internal( "profile/fi/biography/" + president + ".txt" ) );
		}
	}
	
	public void load(){
		manager.load(chalkboardBG);
		manager.load(chalkboardBGFlip);
		manager.load(backToTimelineEn);
		manager.load(backToTimelineFi);
		manager.load(polaroid700x700);
		manager.load(nextBlack);
		manager.load(prevBlack);
		manager.load(closeEn);
		manager.load(closeFi);
		manager.load(nextWhite);
		manager.load(prevWhite);
		manager.load(timelineHeaderEn);
		manager.load(takeTheQuizEn);
		manager.load(changeLanguageEn);
		manager.load(timelineHeaderFi);
		manager.load(takeTheQuizFi);
		manager.load(changeLanguageFi);
		manager.load(profileBG);
		manager.load(photoGalleryTextEn);
		manager.load(relevantEventsEn);
		manager.load(speechesEn);
		manager.load(photoGalleryTextFi);
		manager.load(relevantEventsFi);
		manager.load(speechesFi);
		manager.load(pgHeaderEn);
		manager.load(pgHeaderFi);
		manager.load(backToProfileEn);
		manager.load(backToProfileFi);
		manager.load(pgBackground);
		manager.load(pgBackgroundWrap);
		manager.load(frameTop);
		manager.load(frameLeft);
		manager.load(frameRight);
		manager.load(frameBottom);
		manager.load(relevantEventsBG);
		manager.load(nextBrown);
		manager.load(prevBrown);
		manager.load(speechBG);
		manager.load(aButton);
		manager.load(bButton);
		manager.load(cButton);
		manager.load(whiteBox);
		manager.load(overlayTexture);
		manager.load(scrollBG);
		manager.load(getStartedEn);
		manager.load(introQuizBGEn);
		manager.load(getStartedFi);
		manager.load(introQuizBGFi);
		manager.load(whiteBackToTimelineEn);
		manager.load(whiteBackToTimelineFi);
		manager.load(wrongBGEn);
		manager.load(wrongBGFi);
		manager.load(wrongTakeQuizAgainEn);
		manager.load(wrongTakeQuizAgainFi);
		manager.load(quizCompleteBGEn);
		manager.load(quizCompleteBGFi);
		
		for(AssetDescriptor<Texture> presidentPortrait: presidentPortraits)
			manager.load(presidentPortrait);
		for(AssetDescriptor<Texture> presidentName: presidentNames)
			manager.load(presidentName);
		for(AssetDescriptor<Texture> presidentTermEn: presidentTermsEn)
			manager.load(presidentTermEn);
		for(AssetDescriptor<Texture> presidentTermFi: presidentTermsFi)
			manager.load(presidentTermFi);
		for(AssetDescriptor<Texture> profilePortrait: profilePortraits)
			manager.load(profilePortrait);
		for(AssetDescriptor<Texture> snapshot: snapshots)
			manager.load(snapshot);
		
	}
	
	public void dispose(){
		manager.dispose();
	}

}
