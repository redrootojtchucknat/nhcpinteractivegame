package com.mygdx.game;

// this class describes the event object, used in the relevant events screen
public class Event {
	private String title;
	private String content;
	private String path;
	
	public Event( String title, String content, String path )
	{
		this.setTitle(title);
		this.setContent(content);
		this.setPath(path);
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
}
