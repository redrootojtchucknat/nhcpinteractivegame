package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ProfileScreenFi extends ProfileScreen {
	public ProfileScreenFi( InteractiveGame gam, String presidentName)
	{
		super( gam, presidentName );
		
		biography.setText( game.assets.biographiesFi.get( super.i ).readString() );
		
		// load the images
		backToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.backToTimelineFi) ) ) );
		photoGallery.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.photoGalleryTextFi) ) ) );
		relevantEvents.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.relevantEventsFi) ) ) );
		speeches.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.speechesFi) ) ) );

		backToTimeline.addListener( new ClickListener() {
        	public void clicked( InputEvent event, float x, float y ) {
        		game.closeBackSoundEFfect.play();
        		game.setScreen( game.timelineScreenFi );
        	}
        });
	}
}
