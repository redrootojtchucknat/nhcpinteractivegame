package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class PhotoGalleryScreen implements Screen{
	
	final InteractiveGame game;
	private String presidentName;
	final String name;
	private int offset = 0;
	private int snapshotOffset;
	private int snapshotsLength;
	private int snapshotGroupOffset = 0;
	private int speed;
	private float polaroidX2;
	private float polaroidY2;
	boolean isNextTouchedDown;
	boolean isPrevTouchedDown;
	
	private Stage stage;
	private Table table;
	
	private static Texture pgBG;
	private static Texture pgBGWrap;
	
	private ArrayList<Snapshot> snapshotFiles;
	
	private Rectangle photoGalleryBG;
	private Rectangle photoGalleryBGWrap;
	
	private Image header;
	private Image backToProfile;
	private Image frameTop;
	private Image frameLeft;
	private Image frameRight;
	private Image frameBottom;
	private Image photoGalNext;
	private Image photoGalPrev;
	
	private ArrayList<Image> snapshots;
	private ArrayList<Image> polaroids;
	
	private Group polaroidSnapshotGroup;
	
	public PhotoGalleryScreen(final InteractiveGame gam, String presidentName){
		int i, parity;
		game = gam;
		this.presidentName = presidentName;
		
		snapshotOffset = 370;
        speed = 12;
        polaroidY2 = 492;
        polaroidX2 = 662;
        
        
        //allocate space for all
        pgBG = game.assets.manager.get( game.assets.pgBackground );
        pgBGWrap = game.assets.manager.get(game.assets.pgBackgroundWrap);
        snapshotFiles = new ArrayList<Snapshot>();
        snapshots = new ArrayList<Image>();
        polaroids = new ArrayList<Image>();
        stage = new Stage();
        table = new Table();
        
        
        //define the properties of the photo gallery background, frame and buttons
        
        photoGalleryBG = new Rectangle();
        photoGalleryBGWrap = new Rectangle();
        
        header = new Image( game.assets.manager.get(game.assets.pgHeaderEn) );
        backToProfile = new Image( game.assets.manager.get(game.assets.backToProfileEn) );
        photoGalNext = new Image( game.assets.manager.get(game.assets.nextBlack) );
        photoGalPrev = new Image( game.assets.manager.get(game.assets.prevBlack) );
        frameTop = new Image( game.assets.manager.get(game.assets.frameTop) );
        frameLeft = new Image( game.assets.manager.get(game.assets.frameLeft) );
        frameRight = new Image( game.assets.manager.get(game.assets.frameRight) );
        frameBottom = new Image( game.assets.manager.get(game.assets.frameBottom) );
        
        polaroidSnapshotGroup = new Group();
        
        //load properties for all
        photoGalleryBG.x = 0;
        photoGalleryBG.y = 0;
        photoGalleryBGWrap.x = game.windowWidth;
        photoGalleryBGWrap.y = 0;
        frameTop.setX(0);
        frameTop.setY(game.windowHeight - frameTop.getHeight());
        frameLeft.setX(0);
        frameLeft.setY(0);
        frameRight.setX(game.windowWidth - frameRight.getWidth());
        frameRight.setY(0);
        frameBottom.setX(0);
        frameBottom.setY(0);
        
        
        //add the listeners for the table's actors
        photoGalNext.addListener(new ClickListener() {
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		isNextTouchedDown = true;
        		return true;
        	}
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        		isNextTouchedDown = false;
        	}
        	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        		isNextTouchedDown = false;
        	}
        });
        
        photoGalPrev.addListener(new ClickListener() {
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		isPrevTouchedDown = true;
        		return true;
        	}
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        		isPrevTouchedDown = false;
        	}
        	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        		isPrevTouchedDown = false;
        	}
        });
        
        //get snapshots for the corresponding president
        snapshotFiles = game.utility.loadAllSnapshots( presidentName );
        for(Snapshot snapshotFile: snapshotFiles){
        	snapshots.add( new Image( snapshotFile.getSnapshot() ) );
        	polaroids.add( new Image( game.assets.manager.get( game.assets.polaroid700x700) ) );
        }
        	
        i = 200;
        parity = 1;
        name = presidentName;
        int k = 0;
        for( k=0;k<snapshots.size();k++ ){
        	polaroids.get(k).setWidth(polaroids.get(k).getWidth() / 2);
        	polaroids.get(k).setHeight(polaroids.get(k).getHeight() / 2);
            float tempY2 = polaroidY2 / 2;
            float tempX2 = polaroidX2 / 2;
            float ratioX = tempX2/snapshots.get(k).getWidth();
            if(snapshots.get(k).getWidth()>snapshots.get(k).getHeight()){
            	snapshots.get(k).setHeight( tempY2 );	// scale to fit screen
            }
            else{
            	snapshots.get(k).setHeight( (ratioX) * snapshots.get(k).getHeight() );
            	if(snapshots.get(k).getHeight()>polaroids.get(k).getHeight())
            		snapshots.get(k).setHeight(polaroids.get(k).getHeight());
            		
            }
            snapshots.get(k).setWidth( tempX2 );	// scale to fit screen	
            polaroids.get(k).setX( i );
            snapshots.get(k).setX( i + (15/2) );
            //portrait up
        	if( parity % 2 != 0 ){
        		polaroids.get(k).setY(game.windowHeight / 2 - game.windowHeight / 12);
        		if(snapshots.get(k).getHeight() + 45 >= polaroids.get(k).getHeight())
        			snapshots.get(k).setY(polaroids.get(k).getY());
        		else
        			snapshots.get(k).setY(polaroids.get(k).getY() + 163/2 - (snapshots.get(k).getHeight()-tempY2));
        	}
        	// portrait down
        	else{
        		polaroids.get(k).setY(game.windowHeight / 6);
        		if(snapshots.get(k).getHeight() + 45 >= polaroids.get(k).getHeight())
        			snapshots.get(k).setY(polaroids.get(k).getY());
        		else
        			snapshots.get(k).setY(polaroids.get(k).getY() + 163/2 - (snapshots.get(k).getHeight()-tempY2));
        	}
        
        	final Texture image = snapshotFiles.get(k).getSnapshot();
        	final String text = snapshotFiles.get(k).getCaption();
        	
        	polaroids.get(k).addListener( new ClickListener() {
        		public void clicked( InputEvent event, float x, float y ) {
        			game.shutterClickSoundEffect.play();
        			game.setScreen( new SnapshotPopupScreen( game, name, image, text) );
        		}
        	});
        	
        	
        	
        	polaroidSnapshotGroup.addActor(snapshots.get(k));
        	polaroidSnapshotGroup.addActor(polaroids.get(k));
        	if( !text.equals( "" ) )
        	{
        		Label caption = new Label( null, game.skin, "brightBeautifulSmallMedium" );
        		caption.setPosition( polaroids.get( k ).getX() + polaroids.get( k ).getWidth() - game.padding, polaroids.get( k ).getY() + game.padding * 2 );
        		caption.setAlignment( Align.bottom );
        		caption.setAlignment( Align.right );
        		caption.setText( "See story" );
        		polaroidSnapshotGroup.addActor( caption );
        	}
        	i += snapshotOffset;
        	parity++;
        }
        		
        
    	// formula to calculate the length of the snapshots
        snapshotsLength = (int) ((( snapshots.size() + 1 ) * snapshotOffset ) - game.windowWidth);
        
        table.setFillParent(true);
        stage.addActor(polaroidSnapshotGroup);
        stage.setViewport(game.viewport);
        stage.addActor(frameTop);
        stage.addActor(frameLeft);
        stage.addActor(frameRight);
        stage.addActor(frameBottom);
        stage.addActor(header);
        stage.addActor( table );
        
        table.row().expand();
        table.add( photoGalPrev ).left();
        table.add( photoGalNext ).right();
        table.row().padBottom(game.windowHeight / 100).bottom().padBottom(game.padding).expandX();
		table.add( backToProfile ).left().padLeft(game.padding * 2f).padBottom( game.padding * 2f );
	}
	

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.camera.update();
		
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw(pgBG, photoGalleryBG.x, photoGalleryBG.y);
		game.batch.draw(pgBGWrap, photoGalleryBGWrap.x, photoGalleryBGWrap.y);
		game.batch.enableBlending();
		game.batch.end();
		
		polaroidSnapshotGroup.setX( snapshotGroupOffset );
		
		// if we have reached the left-most edge...
		if( offset >= 0 )
		{
			photoGalPrev.setVisible( false );
			isPrevTouchedDown = false;
		}
		else
			photoGalPrev.setVisible( true );
		// if we have reached the right-most edge...
		if( offset < -snapshotsLength )
		{
			photoGalNext.setVisible( false );
			isNextTouchedDown = false;
		}
		else
			photoGalNext.setVisible( true );
		
		// this block provides the "movement" by clicking the buttons
		if( isNextTouchedDown )
		{
			snapshotGroupOffset -= speed;
			photoGalleryBG.setX(photoGalleryBG.getX() - speed);
			photoGalleryBGWrap.setX(photoGalleryBGWrap.getX() - speed);
			offset -= speed;
		}
		if( isPrevTouchedDown )
		{
			snapshotGroupOffset += speed;
			photoGalleryBG.setX(photoGalleryBG.getX() + speed);
			photoGalleryBGWrap.setX(photoGalleryBGWrap.getX() + speed);
			offset += speed;
		}
		
		// wrap-around going right
		if( photoGalleryBG.getX() <= -game.windowWidth )
			photoGalleryBG.setX( photoGalleryBG.getX() + game.windowWidth * 2 );
		if( photoGalleryBGWrap.getX() <= -game.windowWidth )
			photoGalleryBGWrap.setX( photoGalleryBGWrap.getX() + game.windowWidth * 2 );
		// wrap-around going left
		if( photoGalleryBG.getX() >= game.windowWidth )
			photoGalleryBG.setX( photoGalleryBG.getX() - game.windowWidth * 2 );
		if( photoGalleryBGWrap.getX() >= game.windowWidth )
			photoGalleryBGWrap.setX( photoGalleryBGWrap.getX() - game.windowWidth * 2 );

		stage.act( Gdx.graphics.getDeltaTime());
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
		
	}

	@Override
	public void show() {
		if(game.language.equals("en")){
	        header.setX(game.windowWidth/2 - game.assets.manager.get(game.assets.pgHeaderEn).getWidth()/2);
	        header.setY(game.windowHeight - game.assets.manager.get(game.assets.pgHeaderEn).getHeight());
	        
	        backToProfile.setDrawable(new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.backToProfileEn) )) );
	        header.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.pgHeaderEn)))  );
	        
	        backToProfile.addListener( new ClickListener() {
	        	public void clicked( InputEvent event, float x, float y ) {
	        		
	        		for(ProfileScreenEn pse : game.profileScreensEn){
	        			if(pse.presidentName.equals(presidentName)){
	        				game.closeBackSoundEFfect.play();
	        				game.setScreen( pse );
	        				break;
	        			}
	        		}
	        	}
	        });
	        
		}
		else if(game.language.equals("fi")){
	        header.setX(game.windowWidth/2 - game.assets.manager.get(game.assets.pgHeaderFi).getWidth()/2);
	        header.setY(game.windowHeight - game.assets.manager.get(game.assets.pgHeaderFi).getHeight());
	        
	        backToProfile.setDrawable(new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.backToProfileFi) )) );
	        header.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.pgHeaderFi)))  );
	        
	        backToProfile.addListener( new ClickListener() {
	        	public void clicked( InputEvent event, float x, float y ) {
	        		for(ProfileScreenFi psf : game.profileScreensFi){
	        			if(psf.presidentName.equals(presidentName)){
	        				game.closeBackSoundEFfect.play();
	        				game.setScreen( psf );
	        				break;
	        			}
	        		}
	        		
	        	}
	        });
		}
		
		Gdx.input.setInputProcessor( stage );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
		
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();;
		pgBG.dispose();
		pgBGWrap.dispose();
	}
	
}