package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class TimelineScreen implements Screen {

    final InteractiveGame game;
    
    protected static int offset = 0;
    protected static int presidentGroupOffset = 0;
    protected static Rectangle timeline;
    protected static Rectangle timelineWrap;
    
    protected int portraitsLength;
    protected int portraitOffset;
    protected int speed;
    protected boolean isNextTouchedDown;
    protected boolean isPrevTouchedDown;

    protected Stage stage;
    protected Table table;
    
    protected static Texture timelineTexture;
    protected static Texture timelineTextureWrap;
    protected Texture headerTexture;

    protected static ArrayList<Image> presidents;
    protected static ArrayList<Image> presidentNames;
    protected Image timelineNext;
    protected Image timelinePrev;
    protected Image takeTheQuiz;
    protected Image changeLanguage;
    protected ArrayList<Image> presidentTerms;
    
    protected Group presidentGroup;

    public TimelineScreen(final InteractiveGame gam) {
    	int i, parity;
        game = gam;
        
        portraitOffset = 450;
        speed = 12;

        stage = new Stage();
        
        // allocate space for these attributes
        presidents = new ArrayList<Image>();
        presidentTerms = new ArrayList<Image>();
        presidentNames = new ArrayList<Image>();
        takeTheQuiz = new Image();
        changeLanguage = new Image();
        
        // define the properties of the timeline background
        timelineTexture = game.assets.manager.get(game.assets.chalkboardBG);
        timelineTextureWrap = game.assets.manager.get(game.assets.chalkboardBGFlip);
        timeline = new Rectangle();
        timeline.width = 1920;
        timeline.height = 1080;
        timeline.x = 0;
        timeline.y = 0;
        timelineWrap = new Rectangle();
        timelineWrap.width = 1920;
        timelineWrap.height = 1080;
        timelineWrap.x = timeline.width;
        timelineWrap.y = 0;
        
        // load the buttons from the skin
        timelineNext = new Image( game.assets.manager.get(game.assets.nextWhite) );
        timelinePrev = new Image( game.assets.manager.get(game.assets.prevWhite) );
        
        // define and add elements to the ArrayLists
        for( String president: game.presidentNames )
        {
        	i = game.presidentNames.indexOf(president);
        	presidents.add( new Image( game.assets.manager.get(game.assets.presidentPortraits.get( i ) ) ) );
        	presidentNames.add( new Image( game.assets.manager.get(game.assets.presidentNames.get( i ) ) ) );
        }
        // set the Y and X coordinates of the Image actors
        i = 0;
        parity = 1;
        for( Image president: presidents )
        {
    		president.setX( i );
    		president.setSize( game.windowWidth * .201875f, game.windowHeight * .46259f);	// scale to fit screen
    		// portrait up
        	if( parity % 2 != 0 )
        		president.setY(game.windowHeight / 2 - game.windowHeight / 12);
        	// portrait down
        	else
        		president.setY(game.windowHeight / 6);
        	i += portraitOffset;
        	parity++;
        }
        
        i = (int) ((presidents.get(0).getWidth()/2) - (presidentNames.get(0).getWidth()/2));
        parity = 1;
        for( Image presidentName: presidentNames )
        {
        	presidentName.setX( i );
    		// portrait up
        	if( parity % 2 != 0 )
        		presidentName.setY(game.windowHeight / 2 - game.windowHeight / 12 - presidentName.getHeight() / 2);
        	// portrait down
        	else
        		presidentName.setY(game.windowHeight / 6 - presidentName.getHeight() / 2);
        	i += portraitOffset;
        	parity++;
        }
        // formula to calculate the length of the portraits
        portraitsLength = (int) ((( presidents.size() + 1 ) * portraitOffset ) - game.windowWidth);
        
        // add the listeners for the table's actors
        timelineNext.addListener(new ClickListener() {
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		isNextTouchedDown = true;
        		return true;
        	}
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        		isNextTouchedDown = false;
        	}
        	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        		isNextTouchedDown = false;
        	}
        });
        
        timelinePrev.addListener(new ClickListener() {
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		isPrevTouchedDown = true;
        		return true;
        	}
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        		isPrevTouchedDown = false;
        	}
        	public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        		isPrevTouchedDown = false;
        	}
        });
        
        takeTheQuiz.addListener( new ClickListener() {
        	public void clicked( InputEvent event, float x, float y ) {
        		game.buttonSoundEffect.play();
        		game.setScreen(game.introQuizScreen);
        	}
        });
        
        changeLanguage.addListener( new ClickListener() {
        	public void clicked( InputEvent event, float x, float y ) {
        		game.buttonSoundEffect.play();
        		game.switchLanguage();
        	}
        });
        
        // add the listeners to change screens when the presidents' portraits are clicked
        for( i = 0; i < presidents.size(); i++ )
        {
        	final int j = i;
        	presidents.get(i).addListener( new ClickListener() {
        		public void clicked( InputEvent event, float x, float y ) {
        			game.portraitSoundEffect.play();
        			if( game.language.equals( "en" ) )
        				game.setScreen( game.profileScreensEn.get(j) );
        			else
        				game.setScreen( game.profileScreensFi.get(j) );
        		}
        	});
        }
        
        // add children to the stage and elements to the table
        table = new Table();
        presidentGroup = new Group();
        table.setFillParent( true );
        stage.setViewport( game.viewport );
        for( Image president: presidents )
        	presidentGroup.addActor( president );
        for( Image presidentName: presidentNames )
        	presidentGroup.addActor( presidentName );
        stage.addActor( presidentGroup );
        stage.addActor( table );
        table.row().expand();
        table.add( timelinePrev ).left();
        table.add( timelineNext ).right();
        table.row().bottom().padBottom( game.padding );
        table.add( takeTheQuiz ).left().padLeft( game.padding * 2f );
        table.add( changeLanguage ).right().padRight( game.padding * 2f );
        
    }
    
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);		
		
		game.batch.setProjectionMatrix(game.camera.combined);
		// draw the header and timeline separate from the stage
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw(timelineTexture, timeline.x, timeline.y);
		game.batch.draw(timelineTextureWrap, timelineWrap.x, timelineWrap.y);
		game.batch.enableBlending();
		game.batch.draw(headerTexture, 0, game.windowHeight - headerTexture.getHeight() - game.padding );
		game.batch.end();

		presidentGroup.setX( presidentGroupOffset );		

		// if we have reached the left-most edge...
		if( offset >= 0 )
		{
			timelinePrev.setVisible( false );
			isPrevTouchedDown = false;
		}
		else
			timelinePrev.setVisible( true );
		// if we have reached the right-most edge...
		if( offset < -portraitsLength )
		{
			timelineNext.setVisible( false );
			isNextTouchedDown = false;
		}
		else
			timelineNext.setVisible( true );
		
		// this block provides the "movement" by clicking the buttons
		if( isNextTouchedDown )
		{
			presidentGroupOffset -= speed;
			timeline.setX(timeline.getX() - speed);
			timelineWrap.setX(timelineWrap.getX() - speed);
			offset -= speed;
		}
		if( isPrevTouchedDown )
		{
			presidentGroupOffset += speed;
			timeline.setX(timeline.getX() + speed);
			timelineWrap.setX(timelineWrap.getX() + speed);
			offset += speed;
		}
		
		// wrap-around going right
		if( timeline.getX() <= -game.windowWidth )
			timeline.setX( timeline.getX() + game.windowWidth * 2 );
		if( timelineWrap.getX() <= -game.windowWidth )
			timelineWrap.setX( timelineWrap.getX() + game.windowWidth * 2 );
		// wrap-around going left
		if( timeline.getX() >= game.windowWidth )
			timeline.setX( timeline.getX() - game.windowWidth * 2 );
		if( timelineWrap.getX() >= game.windowWidth )
			timelineWrap.setX( timelineWrap.getX() - game.windowWidth * 2 );
		
		// let the stage process inputs and draw its elements as well
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
		takeTheQuiz.addAction( Actions.sequence( Actions.moveBy(-takeTheQuiz.getWidth(), 0), Actions.moveBy(takeTheQuiz.getWidth(), 0, 0.5f) ) );
		changeLanguage.addAction( Actions.sequence( Actions.moveBy(changeLanguage.getWidth(), 0), Actions.moveBy(-changeLanguage.getWidth(), 0, 0.5f)));
		Gdx.input.setInputProcessor( stage );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	    timelineTexture.dispose();
	    timelineTextureWrap.dispose();
	    headerTexture.dispose();
	}

}