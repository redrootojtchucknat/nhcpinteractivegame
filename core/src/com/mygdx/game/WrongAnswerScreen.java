package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class WrongAnswerScreen implements Screen{

	final InteractiveGame game;
	private int score;
	
	private Stage stage;
	private Table table;
	private TextureRegion wrongBG;
	private Label scoreMessage;
	private Image wrongBackToTimeline;
	private Image wrongTakeQuizAgain;
	public WrongAnswerScreen( final InteractiveGame gam, int score ){
		game = gam;
		this.score = score;
		
		stage = new Stage();
		table = new Table();
		wrongBG = new TextureRegion();
		wrongBackToTimeline = new Image();
		wrongTakeQuizAgain = new Image();
		
		wrongBackToTimeline.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.buttonSoundEffect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.timelineScreenEn );
				else game.setScreen( game.timelineScreenFi );
			}
		});		
		
		wrongTakeQuizAgain.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.buttonSoundEffect.play();
				game.setScreen( new QuizScreen( game ) );
			}
		});
		
		scoreMessage = new Label( null, game.skin, "eraserBigWhite" );
		
		table.setFillParent( true );
		stage.setViewport( game.viewport );
		stage.addActor( table );
		
		table.defaults().bottom();
		table.add( scoreMessage ).expand().padBottom( game.padding * 6 );
		table.row();
		table.add( wrongTakeQuizAgain ).padBottom( game.padding * 6 );
		table.row();
		table.add( wrongBackToTimeline ).padBottom( game.padding * 4 );		
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);		
		
		game.batch.setProjectionMatrix(game.camera.combined);
		// draw the header and timeline separate from the stage
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw(wrongBG, 0, 0);
		game.batch.enableBlending();
		game.batch.end();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
		
	}

	@Override
	public void show() {
		if(game.language.equals("en")){
			wrongBG.setRegion( game.assets.manager.get(game.assets.wrongBGEn) );
			
			// define the "back to timeline" button
			wrongBackToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.whiteBackToTimelineEn ) ) ) );
	        
	        // define the "take quiz again" button
			wrongTakeQuizAgain.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.wrongTakeQuizAgainEn ) ) ) );

			scoreMessage.setText( "Your score is " + ( ( Integer )score ).toString() );
		}
		else if(game.language.equals("fi")){
			wrongBG.setRegion( game.assets.manager.get(game.assets.wrongBGFi ) );
			
			// define the "back to timeline" button
			wrongBackToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.whiteBackToTimelineFi ) ) ) );
	        
	        // define the "take quiz again" button
			wrongTakeQuizAgain.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.wrongTakeQuizAgainFi ) ) ) );

			scoreMessage.setText( "Ang iskor mo ay " + ( ( Integer )score ).toString() );
		}
		Gdx.input.setInputProcessor( stage );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
