package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

// the loading screen class
public class LoadingScreen implements Screen{
	
	/*
	private static final int    FRAME_COLS = 6;     // #1
    private static final int    FRAME_ROWS = 5;     // #2
    
    Animation           walkAnimation;      // #3
    Texture             walkSheet;      // #4
    TextureRegion[]         walkFrames;     // #5
    TextureRegion           currentFrame;       // #7
    */
	
	final InteractiveGame game;
	private Stage stage;
	private Image loadingBar;
	private Image loadingFrame;
	private Image loadingBarHidden;
	private Image loadingText;
	/*
	private Animation animation;
	private TextureAtlas animPack;
	private float elapsedTime = 0f;
	*/
	
	public LoadingScreen(final InteractiveGame gam){
		game = gam;
		
		/*
		walkSheet = new Texture(Gdx.files.internal("animation.png")); // #9
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);              // #10
        walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                walkFrames[index++] = tmp[i][j];
            }
        }
        walkAnimation = new Animation(0.025f, walkFrames);      // #11
		
		animPack = new TextureAtlas( Gdx.files.internal( "sdaf.pack" ) );
		animation = new Animation( .01f, animPack.getRegions() );
		*/
		
		loadingFrame = new Image( new Texture( Gdx.files.internal( "loading/loading-frame.png" ) ) );
		loadingFrame.setPosition(game.windowWidth/2 - loadingFrame.getWidth()/2, game.windowHeight/2 - loadingFrame.getHeight()/2);
		
		loadingBar = new Image( new Texture( Gdx.files.internal( "loading/loading-bar1.png" ) ) );
		loadingBar.setPosition(loadingFrame.getX() + 15, loadingFrame.getY() + 5);
		
		loadingBarHidden = new Image( new Texture( Gdx.files.internal( "loading/loading-bar-hidden.png" ) ) );
		loadingBarHidden.setPosition(loadingBar.getX(), loadingBar.getY());
		loadingText = new Image( new Texture( Gdx.files.internal( "loading/loading-text.png" ) ) );
		loadingText.setPosition(game.windowWidth/2 - loadingText.getWidth()/2, game.windowHeight/2 + loadingFrame.getHeight()/2);
		
		stage = new Stage();
		stage.setViewport(game.viewport);
		stage.addActor(loadingBar);
		stage.addActor(loadingBarHidden);
		stage.addActor(loadingFrame);
		stage.addActor(loadingText);
	}

	@Override
	public void render(float delta) {
		float percent;
		Gdx.gl.glClearColor(0, 0, 0, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);		
		game.camera.update();
		game.batch.setProjectionMatrix(game.camera.combined);
		/*
		game.batch.begin();
		elapsedTime += Gdx.graphics.getDeltaTime();
		game.batch.draw( walkAnimation.getKeyFrame(elapsedTime, true), game.windowWidth * .5f - walkAnimation.getKeyFrame( elapsedTime ).getRegionWidth() * .5f, game.windowHeight * .65f );  // #16
		game.batch.end();
		*/
		// if the assets manager has finished loading, initialise the screens
		if(game.assets.manager.update()){
			game.loadScreens();
		}
		percent = game.assets.manager.getProgress();
		// this line sets the loading bar to update based on the percent loaded by the asset manager
		loadingBarHidden.setX(loadingBar.getX() + percent * ( ( loadingBar.getWidth()+loadingBar.getX() ) - loadingBar.getX() ) );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		for( Actor actor: stage.getActors() )
			actor.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
}
