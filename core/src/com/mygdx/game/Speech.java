package com.mygdx.game;

public class Speech {
	private String title;
	private String date;
	private String content;
	private String audioFile;
	
	public Speech( String title, String date, String content, String audioFile )
	{
		this.title = title;
		this.date = date;
		this.content = content;
		this.setAudioFile(audioFile);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAudioFile() {
		return audioFile;
	}

	public void setAudioFile(String audioFile) {
		this.audioFile = audioFile;
	}
}
