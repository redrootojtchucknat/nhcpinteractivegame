package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class QuizCompletedScreen implements Screen{
	final InteractiveGame game;
	private Stage stage;
	private Table table;
	private TextureRegion quizCompleteBG;
	private Image quizCompleteBackToTimeline;

	public QuizCompletedScreen( final InteractiveGame gam ){
		game = gam;
		
		stage = new Stage();
		table = new Table();
		quizCompleteBG = new TextureRegion();
		quizCompleteBackToTimeline = new Image();
		
		quizCompleteBackToTimeline.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.buttonSoundEffect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.timelineScreenEn );
				else game.setScreen( game.timelineScreenFi );
			}
		});		
		
		
		table.setFillParent( true );
		stage.setViewport( game.viewport );
		stage.addActor( table );
		table.row().bottom().expandY();
		table.add( quizCompleteBackToTimeline ).padBottom( game.padding * 20 );

	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);		
		
		game.batch.setProjectionMatrix(game.camera.combined);
		// draw the header and timeline separate from the stage
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw(quizCompleteBG, 0, 0);
		game.batch.enableBlending();
		game.batch.end();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update( width, height, true );
		
	}

	@Override
	public void show() {
		if(game.language.equals("en")){
			quizCompleteBG.setRegion( game.assets.manager.get(game.assets.quizCompleteBGEn) );
			
			// define the "back to timeline" button
			quizCompleteBackToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get( game.assets.whiteBackToTimelineEn ) ) ) );
	        
		}
		else if(game.language.equals("fi")){
			quizCompleteBG.setRegion( game.assets.manager.get(game.assets.quizCompleteBGFi ) );
			
			// define the "back to timeline" button
			quizCompleteBackToTimeline.setDrawable( new TextureRegionDrawable( new TextureRegion( game.assets.manager.get(game.assets.whiteBackToTimelineFi ) ) ) );
	        
		}
		Gdx.input.setInputProcessor( stage );
		stage.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
