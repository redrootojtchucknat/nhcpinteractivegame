package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

// the game class, holding all the variables needed across screens
public class InteractiveGame extends Game {
	// define variables to be used in the lifetime of the game
    public final int windowWidth = 1920;
    public final int windowHeight = 1080;
    public final OrthographicCamera camera = new OrthographicCamera();
    public final FitViewport viewport = new FitViewport( windowWidth, windowHeight, camera );
    public final int padding = windowHeight / 80;
    
	public String language = "en";
    public SpriteBatch batch;
    
    public Assets assets;
    public Utility utility;
    public ArrayList<String> presidentNames;
    public Skin skin;
    public Music backgroundMusic;
    public Sound portraitSoundEffect;
    public Sound buttonSoundEffect;
    public Sound pageFlipSoundEffect;
    public Sound shutterClickSoundEffect;
    public Sound correctSoundEffect;
    public Sound wrongSoundEffect;
    public Sound closeBackSoundEFfect;
    
    public TimelineScreenEn timelineScreenEn;
    public TimelineScreenFi timelineScreenFi;
    public IntroQuizScreen introQuizScreen;
    public ArrayList<ProfileScreenEn> profileScreensEn;
    public ArrayList<ProfileScreenFi> profileScreensFi;
    public ArrayList<PhotoGalleryScreen> photoGalleryScreens;
    public ArrayList<SpeechScreenEn> speechScreensEn;
    public ArrayList<SpeechScreenFi> speechScreensFi;
    public ArrayList<RelevantEventsScreenEn> relevantEventsScreensEn;
    public ArrayList<RelevantEventsScreenFi> relevantEventsScreensFi;
    public LoadingScreen loadingScreen;
    public QuizCompletedScreen quizCompletedScreen;
    
    public void create() {
        batch = new SpriteBatch();
        utility = new Utility();
        presidentNames = utility.getPresidentNames();
        
        //define bg music and sound effects
        backgroundMusic = Gdx.audio.newMusic( Gdx.files.internal( "data/bgmusic.mp3" ) );
        portraitSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/button-3.wav" ) );
        buttonSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/button-5.wav" ) );
        pageFlipSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/page-flip-1.wav" ) );
        shutterClickSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/shutter-click-1.wav" ) );
        correctSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/correct.wav" ) );
        wrongSoundEffect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/wrong.wav" ) );
        closeBackSoundEFfect = Gdx.audio.newSound( Gdx.files.internal( "data/soundEffects/button-2.wav" ) );
        
        // define fonts and other stuff to be added to the skin
		BitmapFont pencilPeteFont = new BitmapFont( Gdx.files.internal( "data/fonts/pencilPete144.fnt" ) );
		BitmapFont eraserFont = new BitmapFont( Gdx.files.internal( "data/fonts/eraser144.fnt" ) );
		BitmapFont brightBeautifulFont = new BitmapFont( Gdx.files.internal( "data/fonts/brightBeautiful144.fnt" ) );
		BitmapFont sylfaenFont = new BitmapFont( Gdx.files.internal( "data/fonts/sylfaen144.fnt" ) );
		BitmapFont twCenFont = new BitmapFont( Gdx.files.internal( "data/fonts/twCen144.fnt" ) );
		BitmapFont chandaFelizFont = new BitmapFont( Gdx.files.internal( "data/fonts/chandaFeliz144.fnt" ) );
		TiledDrawable scrollBG = new TiledDrawable( new TextureRegion( new Texture( Gdx.files.internal( "photoGallery/scrollBG.png" ) ) ) );
		
		// define the skin and add elements
		skin = new Skin();
		skin.addRegions( new TextureAtlas( Gdx.files.internal( "data/uiskin.atlas" ) ) );
		skin.addRegions( new TextureAtlas( Gdx.files.internal( "speech/audioButtons.pack" ) ) );
		skin.add( "scrollBG", scrollBG );
		skin.add( "eraserSmall", eraserFont );
		skin.add( "eraserMedium", eraserFont );
		skin.add( "eraserBig", eraserFont );
		skin.add( "pencilPete", pencilPeteFont );
		skin.add( "brightBeautifulSmall", brightBeautifulFont );
		skin.add( "brightBeautifulSmallMedium", brightBeautifulFont );
		skin.add( "brightBeautifulMedium", brightBeautifulFont );
		skin.add( "brightBeautifulBig", brightBeautifulFont );
		skin.add( "sylfaenSmall", sylfaenFont );
		skin.add( "sylfaenMedium", sylfaenFont );
		skin.add( "twCenSmall", twCenFont );
		skin.add( "twCenMedium", twCenFont );
		skin.add( "twCenBig", twCenFont );
		skin.add( "chandaFelizBig", chandaFelizFont );
		
		skin.load(Gdx.files.internal("data/uiskin.json"));
		
		// scale the fonts
		skin.getFont( "eraserSmall" ).setScale( .75f * 2f / 9f );
		skin.getFont( "eraserMedium" ).setScale( .75f * 9f / 25f );
		skin.getFont( "eraserBig" ).setScale( 1f / 2f );
		skin.getFont( "pencilPete" ).setScale( 1f / 2f );
		skin.getFont( "brightBeautifulSmall" ).setScale( .75f * 1f / 3f );
		skin.getFont( "brightBeautifulSmallMedium" ).setScale( .75f * 1.25f / 3f );
		skin.getFont( "brightBeautifulMedium" ).setScale( .75f * 1f / 2f );
		skin.getFont( "brightBeautifulBig" ).setScale( .75f * 3f / 4f );
		skin.getFont( "twCenSmall" ).setScale( .75f * 1f / 3f );
		skin.getFont( "twCenMedium" ).setScale( .75f * 1f / 2f );
		skin.getFont( "twCenBig" ).setScale( .75f * 3f / 4f );
		skin.getFont( "sylfaenSmall" ).setScale( 1f / 3f );
		skin.getFont( "sylfaenMedium" ).setScale( 1f / 2f );
		skin.getFont( "chandaFelizBig" ).setScale( 3f / 4f );
		
        loadingScreen = new LoadingScreen( this );
        assets = new Assets( this );
        assets.load();
        setScreen(loadingScreen);
    }
    
    // function to instantiate all screens
    public void loadScreens(){
    	assets.manager.finishLoading();
    	
    	profileScreensEn = new ArrayList<ProfileScreenEn>();
        profileScreensFi = new ArrayList<ProfileScreenFi>();
        photoGalleryScreens = new ArrayList<PhotoGalleryScreen>();
        speechScreensEn = new ArrayList<SpeechScreenEn>();
        speechScreensFi = new ArrayList<SpeechScreenFi>();
        relevantEventsScreensEn = new ArrayList<RelevantEventsScreenEn>();
        relevantEventsScreensFi = new ArrayList<RelevantEventsScreenFi>();
        
        // generate instances of the screens
        timelineScreenEn = new TimelineScreenEn( this );
        timelineScreenFi = new TimelineScreenFi( this );
        introQuizScreen = new IntroQuizScreen( this );
        quizCompletedScreen = new QuizCompletedScreen( this );
        for( String presidentName: presidentNames )
        {
        	profileScreensEn.add( new ProfileScreenEn( this, presidentName ) );
        	profileScreensFi.add( new ProfileScreenFi( this, presidentName ) );
        	photoGalleryScreens.add( new PhotoGalleryScreen( this, presidentName ) );
        	speechScreensEn.add( new SpeechScreenEn( this, presidentName ) );
        	speechScreensFi.add( new SpeechScreenFi( this, presidentName ) );
        	relevantEventsScreensEn.add( new RelevantEventsScreenEn( this, presidentName ) );
        	relevantEventsScreensFi.add( new RelevantEventsScreenFi( this, presidentName ) );
        }
        
        //play background music
        backgroundMusic.setLooping(true);
        backgroundMusic.play();
        this.setScreen( timelineScreenEn );
    }
    public void render() {
        super.render(); //important!
    }

    public void dispose() {
    	backgroundMusic.dispose();
    	portraitSoundEffect.dispose();
    	buttonSoundEffect.dispose();
    	pageFlipSoundEffect.dispose();
    	shutterClickSoundEffect.dispose();
    	correctSoundEffect.dispose();
    	wrongSoundEffect.dispose();
    	closeBackSoundEFfect.dispose();
        batch.dispose();
        skin.dispose();
        assets.dispose();
        timelineScreenEn.dispose();
        timelineScreenFi.dispose();
        introQuizScreen.dispose();
        for( ProfileScreen profileScreen: profileScreensEn )
        	profileScreen.dispose();
        for( ProfileScreen profileScreen: profileScreensFi )
        	profileScreen.dispose();
        for( PhotoGalleryScreen photoGalleryScreen: photoGalleryScreens )
        	photoGalleryScreen.dispose();
        for( SpeechScreenEn speechScreenEn: speechScreensEn )
        	speechScreenEn.dispose();
        for( SpeechScreenFi speechScreenFi: speechScreensFi )
        	speechScreenFi.dispose();
        for( RelevantEventsScreenEn relevantEventsScreenEn: relevantEventsScreensEn )
        	relevantEventsScreenEn.dispose();
        for( RelevantEventsScreenFi relevantEventsScreenFi: relevantEventsScreensFi )
        	relevantEventsScreenFi.dispose();
        loadingScreen.dispose();
        quizCompletedScreen.dispose();
    }
    
    public void switchLanguage()
    {
    	if( language.equals( "en" ) )
    	{
    		language = "fi";
    		this.setScreen( timelineScreenFi );
    	}
    	else
    	{
    		language = "en";
    		this.setScreen( timelineScreenEn );
    	}
    }
}