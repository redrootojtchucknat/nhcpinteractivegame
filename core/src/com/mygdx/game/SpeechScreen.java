package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class SpeechScreen implements Screen {
	final InteractiveGame game;
	final int i;
	
	protected int selector;
	protected Stage stage;
	protected Table table;
	
	private static Texture backgroundTexture;
	
	protected TextureRegionDrawable closeTextureRegionDrawable;
	
	protected Label speechHeader;
	protected Label name;
	protected ArrayList<Speech> speeches;
	protected ArrayList<Label> speechTitles;
	protected ArrayList<Label> speechDates;
	protected ArrayList<Label> speechContents;
	protected ArrayList<ScrollPane> speechScrollPanes;
	protected ArrayList<Music> speechRecordings;
	protected Image prev;
	protected Image next;
	protected Image close;
	protected Image snapshot;
	protected Image polaroid;
	protected Group polaroidSnapshot;
	protected Button play;
	protected Button pause;
	protected Button stop;
	
	public SpeechScreen( final InteractiveGame gam, String presidentName )
	{
		game = gam;
		i = game.presidentNames.indexOf( presidentName );
		int k;
		selector = 0;
		
		stage = new Stage();
		speeches = new ArrayList<Speech>();
		speechTitles = new ArrayList<Label>();
		speechDates = new ArrayList<Label>();
		speechContents = new ArrayList<Label>();
		speechScrollPanes = new ArrayList<ScrollPane>();
		speechRecordings = new ArrayList<Music>();
		polaroidSnapshot = new Group();
		
		backgroundTexture = game.assets.manager.get(game.assets.speechBG);
		
		speeches = game.utility.loadAllSpeeches( presidentName );
		
		play = new Button( game.skin, "play" );
		play.setPosition( game.windowWidth * .55f, game.windowHeight * .1f );
		
		stop = new Button( game.skin, "stop" );
		stop.setPosition( game.windowWidth * .6125f, game.windowHeight * .1f );
		
		pause = new Button( game.skin, "pause" );
		pause.setPosition( game.windowWidth * .55f, game.windowHeight * .1f );
		
		play.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.backgroundMusic.pause();
				speechRecordings.get( selector ).play();
				play.setVisible( false );
				pause.setVisible( true );
			}
		});
		
		pause.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.backgroundMusic.play();
				speechRecordings.get( selector ).pause();
				pause.setVisible( false );
				play.setVisible( true );
			}
		});
		
		stop.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				game.backgroundMusic.play();
				speechRecordings.get( selector ).stop();
				pause.setVisible( false );
				play.setVisible( true );
			}
		});
		
		// close button
		close = new Image();

		// polaroid backdrop
		polaroid = new Image( game.assets.manager.get(game.assets.polaroid700x700) );
		polaroid.setSize( polaroid.getWidth() * .725f, polaroid.getHeight() );
		
		// picture of president
		snapshot = new Image( game.assets.manager.get(game.assets.snapshots.get( i )) );
		snapshot.setPosition( ( polaroid.getWidth() - snapshot.getWidth() )/ 2, polaroid.getY() + 163 / 2 );
		
		// name label
		name = new Label( presidentName, game.skin, "twCenMedium" );
		if( presidentName.length() > 46 )
			name.setStyle( new LabelStyle( game.skin.get( "twCenSmall", LabelStyle.class ) ) );
		name.setWrap( true );
		name.setAlignment( Align.center );
		name.setBounds( 0, 0, polaroid.getWidth(), polaroid.getHeight() * .25f );
		
		// define the english and filipino headers
		speechHeader = new Label(null, game.skin, "twCenMedium");
		speechHeader.setPosition( game.windowWidth * .05f, game.windowHeight * .9f );
		
		// define positions for arrows as well as the arrows themselves
		prev = new Image( game.assets.manager.get(game.assets.prevBlack) );
		next = new Image( game.assets.manager.get(game.assets.nextBlack) );
		prev.setPosition( 0, game.windowHeight * .35f );
		next.setPosition( game.windowWidth * .5f, game.windowHeight * .35f );
		prev.setVisible( false );
		
		
		// define speech content and its scroll pane
		for( k = 0; k < speeches.size(); k++ )
		{
			Label tempSpeechTitle = new Label( null, game.skin, "twCenMedium" );
			Label tempSpeechDate = new Label( null, game.skin, "twCenMedium" );
			Label tempSpeechContent = new Label( null, game.skin, "sylfaenSmall" );
			FileHandle content = Gdx.files.internal( "speech/speeches/" + presidentName + "/" +  speeches.get( k ).getContent());

			int titleLength = speeches.get( k ).getTitle().length();
			if( titleLength > 60 )
			{
				tempSpeechTitle.setStyle( new LabelStyle( game.skin.get( "twCenSmall", LabelStyle.class ) ) );
			}
			tempSpeechContent.setWrap( true );
			tempSpeechTitle.setWrap( true );
			
			tempSpeechTitle.setBounds( game.windowWidth * .05f, game.windowHeight * .75f, game.windowWidth * .475f, 0 );
			tempSpeechDate.setPosition( game.windowWidth * .2875f, game.windowHeight * .65f );
			tempSpeechTitle.setAlignment( Align.center );
			tempSpeechDate.setAlignment( Align.center );
			tempSpeechTitle.setText( speeches.get( k ).getTitle() );
			tempSpeechDate.setText( speeches.get( k ).getDate() );
			tempSpeechContent.setText( content.readString() );
			
			speechTitles.add( tempSpeechTitle );
			speechDates.add( tempSpeechDate );
			speechContents.add( tempSpeechContent );
			
			if( speeches.get( k ).getAudioFile() != null )
			{
				Music speechRecording = Gdx.audio.newMusic( Gdx.files.internal( "speech/speeches/" + presidentName + "/audio/" + speeches.get( k ).getAudioFile() ) );
				speechRecording.setLooping( true );
				speechRecordings.add( speechRecording );
			}
			else
			{
				Music speechRecording = null;
				speechRecordings.add( speechRecording );
			}
			
			ScrollPane speechScrollPane = new ScrollPane(speechContents.get( k ), game.skin);
			speechScrollPane.setScrollingDisabled(true, false);
			speechScrollPane.setOverscroll(false, true);
			speechScrollPane.setupFadeScrollBars(0, 0);
			if( k != 0 )
			{
				speechScrollPane.setVisible( false );
				tempSpeechTitle.setVisible( false );
				tempSpeechDate.setVisible( false );
			}
			speechScrollPanes.add( speechScrollPane );
		}
		
		if( speeches.size() == 1 ) next.setVisible( false );
		
		for( k = 0; k < speeches.size(); k++ )
		{
			stage.addActor( speechTitles.get( k ) );
			stage.addActor( speechDates.get( k ) );
			stage.addActor( speechScrollPanes.get( k ) );
		}
				
		close.addListener( new ClickListener() {
			final int j = i;
			public void clicked( InputEvent event, float x, float y )
			{
				game.closeBackSoundEFfect.play();
				if( game.language.equals( "en" ) ) game.setScreen( game.profileScreensEn.get( j ) );
				else game.setScreen( game.profileScreensFi.get( j ) );
			}
		});
		
		prev.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				if( selector > 0 )
				{
					game.pageFlipSoundEffect.play();
					hideCurrentSpeech( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
					selector--;
					resetSpeeches( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
					if( selector == 0 )
						prev.setVisible( false );
					next.setVisible( true );
				}
			}
		});
		
		next.addListener( new ClickListener() {
			public void clicked( InputEvent event, float x, float y )
			{
				if( selector < speechScrollPanes.size() - 1 )
				{
					game.pageFlipSoundEffect.play();
					hideCurrentSpeech( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
					selector++;
					resetSpeeches( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
					if( selector == speechScrollPanes.size() - 1 )
						next.setVisible( false );
					prev.setVisible( true );
				}
			}
		});
		
		polaroidSnapshot = new Group();
		polaroidSnapshot.addActor( snapshot );
		polaroidSnapshot.addActor( polaroid );
		polaroidSnapshot.addActor( name );
		polaroidSnapshot.setRotation( -4f );
		polaroidSnapshot.setPosition( game.windowWidth * .6f, game.windowHeight * .3f );
		stage.setViewport( game.viewport );
		stage.addActor( close );
		stage.addActor( speechHeader );
		stage.addActor( prev );
		stage.addActor( next );
		stage.addActor( polaroidSnapshot );
		stage.addActor( play );
		stage.addActor( pause );
		stage.addActor( stop );
	}
	
	public void resetSpeeches( ScrollPane ssp, Label title, Label date, Music music )
	{
		ssp.setScrollY( 0 );
		ssp.setBounds(game.windowWidth * .06875f, game.windowHeight * .05f, game.windowWidth * .4375f, game.windowHeight * .55f);
		ssp.setVisible( true );
		title.setVisible( true );
		date.setVisible( true );
		if( music != null )
		{
			play.setVisible( true );
			pause.setVisible( false );
			stop.setVisible( true );
		}
		else
		{
			play.setVisible( false );
			pause.setVisible( false );
			stop.setVisible( false );
		}
	}
	
	public void hideCurrentSpeech( ScrollPane ssp, Label title, Label date, Music music )
	{
		ssp.setVisible( false );
		title.setVisible( false );
		date.setVisible( false );
		if( music != null )
		{
			game.backgroundMusic.setVolume( 1.0f );
			music.stop();
		}
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		game.batch.setProjectionMatrix(game.camera.combined);
		game.batch.begin();
		game.batch.disableBlending();
		game.batch.draw( backgroundTexture, 0, 0 );
		game.batch.enableBlending();
		game.batch.end();
		
		stage.act( Gdx.graphics.getDeltaTime() );
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.getViewport().update( width, height, true );
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor( stage );
		hideCurrentSpeech( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
		selector = 0;
		resetSpeeches( speechScrollPanes.get( selector ), speechTitles.get( selector ), speechDates.get( selector ), speechRecordings.get( selector ) );
		prev.setVisible( false );
		next.setVisible( true );
		if( speeches.size() == 1 )
			next.setVisible( false );
		for( Actor actor: stage.getActors() ){
			if(!actor.equals(polaroidSnapshot)){
				actor.addAction( Actions.sequence( Actions.fadeOut( 0.001f ), Actions.fadeIn( 0.5f ) ) );
			}
			
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		Music music = speechRecordings.get( selector );
		if( music != null )
		{
			game.backgroundMusic.play();
			music.stop();
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
		backgroundTexture.dispose();
	}

}
