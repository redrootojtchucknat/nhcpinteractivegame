President Ramon Magsaysay lies in state in the Ceremonial Hall of Malaca�an Palace, March 1957.

In accordance with Military Honors, the flag is draped on the casket and it should be of adequate size to drape properly.
 
(Photo courtesy of LIFE Magazine)