Ramon Magsaysay, Defense Secretary for President Elpidio Quirino, marking a Huk target with a smoke bomb from a spotter plane.

Ramon Magsaysay was appointed Secretary of National Defense by President Elpidio Quirino, on August 31, 1950.
 
Photo courtesy of "LIFE Magazine"