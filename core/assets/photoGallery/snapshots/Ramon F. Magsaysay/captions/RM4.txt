President-elect Ramon Magsaysay tries out the presidential chair

President-elect Ramon Magsaysay tries out the presidential chair, on the invitation of President Elpidio Quirino, when Magsaysay arrived to fetch the latter on inaugural day. Taken on December 30, 1953.
 
Photo taken from "Palacio de Malacaņang"