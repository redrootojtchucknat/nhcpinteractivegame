Ramon Magsaysay, man of the masses

"The mistake the world is making with simple peoples is to try to hurry them into political concepts they don�t understand and aren�t prepared to cope with. I know, for I am a peasant myself." - President Ramon Magsaysay
 
Photo taken from "Magsaysay: The People's President."