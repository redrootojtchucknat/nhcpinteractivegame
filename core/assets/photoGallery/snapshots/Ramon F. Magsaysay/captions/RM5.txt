President Ramon Magsaysay with his visitors at Malaca�an

The usual sight in Malaca�an Palace during Ramon Magsaysay�s term. As noted by the Philippines Free Press, the very first �at home� of President Magsaysay in residence (a mere two days after his inauguration) had Malaca�an Palace opening its doors to citizens�and they came en masse.
 
Photo taken from "Magsaysay: The People's President."