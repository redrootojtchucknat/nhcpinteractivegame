On 30 June 1958, Philippine President Carlos P. Garcia and his wife, the former Leonila Dimataga, returning from Washington, DC via San Francisco, stopped for a visit in Honolulu. 
This picture was taken at Hickam Air Force Base on that day. 
