My beloved and fellow Countrymen:

By the generosity of your sovereign mandate, I have been sworn again today before you, and in the sight of the world to defend, preserve, and promote all that we hold dear and cherish as a nation.

The greatness of this occasion dwarfs all personal feelings of joy, gratitude and pride which one feels at this moment. As it has ever been, the entire nation, not one man alone, is summoned by this historic ceremony to ledge faith in ourselves and in the future.

In the history of our nation, there are certain moments that fix for us a tide of turning and redirection, when the nation irrevocably moves into a new course and bravely ventures into the future.

This is such a turning point.

On a dreary December dawn eighty-five years ago, young patriot and prophet of the race fell on this very soil, �a martyr to his illusions� of a new Philippines.

Sixteen years ago, we stood on this same hallowed ground, sharing a vision of Filipino greatness.

Sixty-nine years separated the martyrdom and the mandate of greatness. More than two generations of thwarted hopes and disappointed dreams. The blood of the martyr inspired the Philippine revolution, from which sprung the first Asian Republic, proclaimed in Kawit, Cavite on the twelfth of June, 1898. It was a short - lived Republic, for soon after, recalling three and half centuries of foreign domination, we were colonized once again.

Two other republics followed, under which the Philippines was independent but not truly sovereign, for as history decrees, authentic freedom is self-proclaimed and is not a gift from alien lands.

The Filipino had lost his dignity, his courage and even his soul. For we existed at the time in a precarious democracy which advanced the few who were rich and powerful and debased the many who were poor. Government was the acolyte of an oligarchy whose preeminence reached back to the colonial era. This arrangement resulted in a society that could no longer endure; as social scientists said, we, the Filipinos, were sitting on top of a �social volcano.� But our young martyr and hero had a more arresting metaphor. A social cancer was all over the land. A major surgery was inevitable.

We were caught, according to a popular image, between a world that was dead and a world that was too feeble to be born. We had to accelerate the birth of this New World�a Caesarean operation was required. Either that or perish beneath the weight of our own failures.

Out of this peril�from the jaws of dismemberment and extinction, and from the heart of the rebellion of the poor�was our new republic.

The first republic was still�born, it had to be born again�in our time.

Today, we proclaimed here the birth of a new republic, new in structure and character, and ordained to preside over a new time of ferment and change in our national life.

Our republic is new for its fidelity to our historical legacy and its utter repudiation of the colonial past. In it is the power of the vision which sacrificed the �First Filipino� on this sacred ground. Time alone can tell the fate and fortune of our new republic.

Thus has history presented our saga to this generation of Filipinos that we must rise from the depths of ignominy and failure; and thus is it said that we have an appointment with destiny.

With the past, we affirm here and now the continuity and integrity of the nation vouchsafed to us by the sacrifices and struggles of our fathers. But we are also deeply conscious of the need to break away from the historical ties that have fettered time and again the pursuit of the national destiny.

All too often in the long sweep of history, we have seen national longing and aspiration denied at the threshold of fulfillment. We have seen our nation aborted at birth, tossed in the sway of empires, gripped in irresolution and drift, paralyzed by disunity and strife, and hostage to chaos and instability. Our national independence since the beginning has been partly cast in light, partly in shadow.

There are, to be sure, imperfections in our institutions, and as these are run by men, there are more imperfections still. But I choose to regard these as redeemable; we can still win the few, the faltering, recalcitrant few, to our government.

Unerringly, the many crises and trials of our republic have repeatedly pointed to one recurring theme. The helplessness of government to cope with problems and its inability to prosecute national purposes and goals. We have suffered less from the failure of political ideals than from the failure to make democratic government work and prosper in our land.

From such failure did we pass into the long night of crises and instability that so lately visited our land, and required the extraordinary recourse to martial law and the establishment of a crisis government.

Yet from our response to that time of challenge, during an eight-and-a-half year period that will ever be distinguished landmark in the history of our nation, we emerged a nation strengthened and transformed, her faith renewed in the vitality of her democratic institutions

Ironic, we say now, is the fact that to arrive at this new beginning for democracy in our country, we have had to travel the route of authoritarian government, passed through the very eye of hazard and crisis, and endured the verdict of some men who despaired that democracy has been irretrievably lost in our land.

The interval of crisis government and reconstruction opened to our nation a new meaning in the democratic ideal and a new dynamism towards its attainment. We can never again stand in alienation from one another, in resignation before our problems, or in humiliation before the world.

Living through the tempest of crises and ferment, we have known the reserves of national will we possess and the kind of government we are truly capable of establishing in our country.

Not the poverty of principles, or the decay of ideals, but the simple failure of government has undermined our confused and tortuous route as a nation. And so it is that our national rebirth must be founded first and foremost upon the rock of government.

This is the new beginning that we proclaim�today the awakening of our republic to the fundamental challenge of governing our land and our people, of ministering to the cares of public life, and of redeeming every dream and every aspiration that throughout our national history has fired the hearts and minds of our people.

Fundamentally, this is a beginning and a change not in dreams and aspiration, but in rededication and reappraisal.

For so long we have immersed in a confused debate over ideas and principles, when our real need was self-discipline.

We have swayed between the ideologies of the times and been entrapped in their irresoluble contention, when our attention might have been better focused infusing confidence in our people.

We have wearied of the efficacy of democratic ideals, despaired of their ever taking root in our country, when our task was to make these ideals find life and sustenance in our society through our own self-abnegation.

We have been captivated by models and images of development not our own making without moulding them to the reality of our culture and traditions.

Today, we know better.

This is the vision of rebirth that we hold out to the nation today of a new people and a new government that will be stable, strong, and capable of leading that way the national future.

Our chief concern is to develop and perfect the means whereby government may recapture the original purpose of society�that of promoting the well-being of all the members of our community. This is no mere sentiment. This we recognize as a fundamental duty to be practically and resolutely pursued.

Its essence is less to be seen in what we say of those timeless democratic principles we swear by�freedom, rights, morals, service and the like�than in the manner by which we organize law and government for their realization in our society.

Others may speak of their facile ideas to make change and development in our country. We shall address ourselves to the organization of government, to the engineering of change, to the management of our affairs for this is how, fundamentally and truly, the most lofty ideals begins to be realized and come to live in society.

We speak to our farming communities who had long been disenchanted by slogans promising land and progress and who at last, under the 1972 program of land reform now have their own lands to till but also need the continuing assistance of their government to attain both advancement in their lives and growth in their communities.

We speak to our working classes who need not only a greater share in the profits of production but the upgrading of skills and talents by the energetic action of government so that they may carry out the programs designed to spur the growth of our economic and social life.

We speak to the entrepreneurs throughout our land who need, besides the maintenance of our free enterprise system, the practical assistance of government in the identification of markets, in the development of sources of labor and raw materials, in the availment of credit facilities, and in the concerted effort to free the full bounty of our resources as a nation.

We speak to every family�to every man, woman and child throughout our land�whose security, well-being and advancement must be directly affected by, and be the concern of, government in practical programs that will broaden opportunities in education, health and welfare and other human needs.

We speak to the citizenry, whose sovereign will and whose rights must exist not only on paper, but in effective processes that magnify its participation in government and its enjoyment of its rights.

We speak also to the family of nations and the councils of the world, to which our national life is so intimately linked today, to which we pledge continued and abiding cooperation in efforts and programs that will truly advance the peace and progress of peoples, especially of those with whom we share the cause of reform of the international system.

We shall not merely dream, we shall achieve.

We are done with pining for all the comforts and rewards of more advanced societies. We shall now draw up our own plans in accordance with our vision and culture, to realize within our land our own program of development and progress.

True liberation such as has been dreamed of since the birth of our nation, and has never left the bosom of our people, is not to be attained save by the enduring union of government and the people.

It is unthinkable that we should approach this task as partisans to warring interests, creeds and ideologies. Our goal is to unite, not divide.

National unity is a covenant between each and every Filipino, and between the leader and his people. The rare honor that you have bestowed on me as your thrice-elected leader imposes on my person� and those closes to me�a debt, an obligation that I cannot shirk and a pledge that I dare not betray. Let history judge me harshly on this - that until every Filipino can say with conviction that he has been liberated from ignorance, poverty, and disease; until, in sum, he can call his mind, body, and spirit his own, I shall have failed you.

For this purpose it shall be our task as a people to break, with the force of our will and our energies, the tradition of discord and suspicion that characterized our efforts in the past to build one nation.

It is a duty we can no longer ignore or deny to bring the wasteful strife in the South to an end, to settle for all time the secessionist war which has haunted the nation these past several years. Let us sweep aside the gloom of separatism and distrust.

We must apply to the cultivation of a new national tradition of Filipino-unity, in which Christian and Muslim are brothers in blood and aspiration, in which religious freedom is not only a guarantee but also a true and enduring bond to hold all men together, none of them less than the others because of his religious creed or mode of worship.

We ask all our countrymen, every group and every sector of our society, to gather around this work now unfolding, to lend to it their counsel and their guidance, and the light of their earnest criticism.

We have had enough of bitterness and faction among us to realize now that we have spent ourselves and reduced thereby the vitality and strength of our nation. Learning from one another, striving towards consensus, contending and yet aware of our common life as a nation, we can provide healing answers to the travails of national life. We shall move forward together.

Of the leadership, the dedication, and the vision so clearly needed by this work of building and creation in our land, government shall be the first to provide. We shall bring into the service of government the broadest knowledge and expertise available throughout our land. We shall set upon the task of reconstructing on a new foundation the whole of our government bureaucracy�from the lowest echelons to the highest�so that we shall have once and for all truly a government that is servant to our hopes and our needs.

Government can lead the way to building our new republic but it cannot do the task alone. It requires us also to establish new concepts of cooperation and interaction between government and the people, between communities and their leaders, between the variant sectors of our society.

The people�s initiative, their caring, and their imagination, as much as those of government, will determine how far and how fast we can achieve the blessings of true democracy on our land.

But I do believe that our people have never been more prepared for this test of their communal life and for the effort that it demands. I believe that today we all see our problems and our opportunities more clearly than we used to as tasks that are resolved not by a fever of words and hopes but by action patiently applied to them.

When I look upon our history as a nation, it is this attitude to work and struggle that is truly new in our society today. It is this profession of faith above all others that shines upon our work now beginning in our country.

Nearly a century ago, the man who was martyred on these grounds, Dr. Jose Rizal, described in words of cautious prophecy the nation that the Filipino race could become a century hence. He wrote:

�The Philippines will defend with inexpressible valor the liberty secured at the price of so much blood and sacrifice. With the new men that will spring from their soil and with the recollection of their past, they will perhaps strive to enter freely upon the wide road of progress, and all will labor together to strengthen their fatherland, both internally and externally, with the same enthusiasm, with which a youth falls again to tilling the land of his ancestors so long wasted and abandoned through the neglect of those who have withheld it from him. Then the mines will be made to give up their fold for relieving distress, iron for weapons, copper, lead and coal. Perhaps the country will revive the maritime and mercantile life for which the islanders are fitted by their nature, ability and instincts, and once more free, like the bird that leaves its cage, like the flower that unfolds to the air, will recover the pristine virtues that are gradually dying out and will again become addicted to peace - cheerful, happy, joyous, hospitable and daring.�

We are the nation today. With courage and vision we shall be more.

From you therefore, my countrymen, I ask utmost commitment, the unswerving allegiance to the vision which unites us. You owe this to yourselves.

Give all that you can to your country, and I, God willing, will leave you a society that will fill all your needs for a decent and honorable life.

Let us then call on the intransigent to realize their just purpose with us; let us awaken the unconscious and enlighten the mislead; let us listen to our detractors in honest counsel. Let us bind the wounds of the past, and, in one united effort, realize the aspirations of our people. There are no outside saviors; there is only us - the Filipinos.

There is no injustice that we cannot dedicate, no corruption that we cannot extirpate, no hardship or crisis that we cannot overcome as long as we keep faith with the vision of national greatness.

With the advent of this hour of the New Republic, we enter with a clear eye and a stout heart a perilous decade. There is nothing to fear; we shall ahieve national liberation; we shall prevail.

I ask you then: let us cross this frontier.


http://www.gov.ph/1981/06/30/third-inaugural-address-of-president-marcos-june-30-1981/