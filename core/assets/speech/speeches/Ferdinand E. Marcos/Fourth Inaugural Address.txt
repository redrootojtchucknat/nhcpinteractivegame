Here is the text of a speech delivered by Ferdinand E. Marcos after he took an oath of office as president of the Philippines today, an hour after Corazon Aquino took a similar oath:

I accept with humility the honor which the Filipino people and the republic have conferred on me today and I take up this cause and charge with a great resolve to do all those (things) that will be necessary to attain the welfare of the people of the republic.

No man can be more proud than I am at this moment. This moment we are receiving from you, our people, the highest office available in the hands of our people and that is the presidency of the Republic of the Philippines.

The times do not call for a celebration and that is why we are gathered here in this ceremonial hall in austere ceremonies. But it calls for an earnest and serious confrontation of the challenges and problems before us today.

The tasks we face are difficult and they will ask for sacrifices from all of us. We can overcome them only if we are gathered to one single, united nation, stand firm in the faith that constitutional government must reign in this country.

Today, that faith is challenged and it will ask of everyone of us, perhaps especially your humble servant as president of the republic, not only to sacrifice, but perhaps the ability to stand attacks upon not only your lives and property, but most valuable of all, your sacred honor.

Today, the concept of our democracy is under utmost challenge, not only by other ideologies, but by some of our own people who seek to win, perhaps by force and intimidation, the power that they have failed to get and to win in a fair, honest and clean election. And it is under challenge also by those forces in our midst which have long been advocates of violence for the overthrow of the government and the destruction of our democratic society. This we will not allow.

So today, let every man in our republic, whether farmer, laborer, intellectual or ordinary citizen - for he is a part of the building of this nation - let each and everyone, therefore, proclaim that we shall protect and we shall defend that constitution, which establishes not only the democractic institutions of our country, but (also) the freedom and the dignity that we value so much.

From this faith in our democratric processes, we will not be distracted. If there is any victory which you and I can be proud of, it is that victory of changing our people from whining, oppressed (people with) heads bowed down in resignation to desperate hopelessness, and turning them into militant, dynamic, dignified Filipinos.

From this spiritual rejuvenation of our people, whose hearts and souls were withered from long, long years of colonization, we now suddenly discover amongst us a people who would not only love freedom but execise it, and do exercise it, in a very militant form in the utilization of the ballot, which is involved in the selection of its leaders.

For certainly, events have brought our democracy to a new kind of testing, testing as well as of challenge. The circumstances will not only test our courage, (but also) our devotion to duty and our commitment to democracy. Whatever be the challenges, whatever be the obstacles before us, I say to you as I say to everybody else that we will overcome.

Let us, in our faith in the Almighty and strengthened by a united people, in our sense of purpose and brotherhood ... advance toward a future which will be bright and prosperous and, best of all, a future which is of our own making.

As I take the oath today as president of the republic, I promise you only one thing, that the power of the presidency will be utilized in order to free our people from the bondage of the old weaknesses and vices, and we will lift our people. We will utilize the powers of the presidency in order that we can lift our people to the heaven of progress, peace, and security in this land.



http://www.apnewsarchive.com/1986/Text-of-Marcos-Inaugural-Address/id-bc55c5c09a3d87472a4f43fedb5dfa1c