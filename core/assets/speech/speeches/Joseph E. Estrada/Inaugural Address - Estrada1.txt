Good afternoon.

The light is fading, the day is almost over, and yet this late afternoon is the morning of a new day. The day of the Filipino masses. One of their own is finally 
leading them.

The last time I was here at the Quirino grandstand, I was with President Cory Aquino, Cardinal Sin, and other religious leaders and fighters for democracy. We were here with many of you to stand up and be counted as friends of the democracy. Ask yourselves then, how could anyone call me a dictatorial type?

The last time I was there, in the old senate building, we were only Twelve �

Twelve against a superpower;

Twelve against a government under its thumb:

Twelve against public opinion:

But twelve for the sovereignty and honor of our country.

Ask yourselves who has principles.

Maybe I felt strongly about getting all the wrong priorities out of the way so we can focus on the right things at once.

Maybe I felt that we cannot wait for time to heal our wounds and that we should help along the healing process.

Who has been hurt and insulted than I? I have been hurt, and my mother even more deeply at having to listen to all those insults against her son in tri-media. I am but human and I don�t want to forgive. But I must. And I have. I must work with those who hurt me because we have only one country between us. I must work with them and they must live with me, because every Filipino is needed to meet the challenge of national survival in the regional crisis.

If I seemed impatient, it was only for peace. We must put yesterday behind us, so we can work for a brighter tomorrow. I did not mean for us to forget the past. I don�t. But I hope we will not let the past get in the way of a future that calls for cooperation to achieve peace and prosperity.

Finally, I felt that the common people have waited long enough for their turn, for their day to come.

That day is here.

And it comes not a moment too soon on the centennial of the birth of Filipino freedom.

One hundred years after Kawit, fifty years after independence, twelve years after Edsa, and seven years after the rejection of foreign bases, it is now the turn of the masses to experience liberation.

We stand in the shadow of those who fought to make us free�free from foreign domination, free from domestic tyranny, free from superpower dictation, free from economic backwardness. We acknowledge a debt of gratitude to Jose Rizal, Andres Bonifacio, Emilio Aguinaldo, Manuel Quezon, Ramon Magsaysay, Cory Aquino, Fidel Ramos, and the magnificent twelve of the 1991 senate who voted for Filipino sovereignty and honor.

These are the men and women who gave birth to the idea of Filipino freedom; who struggled in war to give it recognition; and worked in peace to make it come true. Cory Aquino brought freedom back after it was taken away and Fidel Ramos showed how power should respect the people�s freedom of choice in elections.

They also began the slow and difficult work of making freedom more meaningful � not just for the rich but also for the poor who are more but have nothing.

It is time. Time to speed up the improvement of the living conditions of the common people. Time for them to have a fairer share of the national wealth they create and a bigger stake in their own country.

Some will say we cannot rush these things. First, focus on the economy again. Of course, we must improve the economy. How else can the people�s lives improve? But why not both together? Why must economic progress always be at the people�s expense?

When it was a question of economic reforms to rebuild business confidence and restore business profits, the reforms were never too fast or too hard, especially for the common people to bear.

Six years after Cory Aquino, the foundations of a strong economy were laid. In the six years of the Ramos administration, the economy was paying big dividends to its biggest stockholders. This time, why not to the common people as well, for a change? Must we always measure progress only by the golf courses of the rich?

I hope this message will not be taken badly by the rich. It has always been their turn, and it is also their turn again. For it is the priority of my administration to create the environment of peace and order in which business does well. But, surely, it is time for the masses to enjoy first priority in the programs of the government.

As far as resources permit, to the best of our ability and the limit of our energy, we will put a roof over their heads, food on their tables and clothes on their backs. We will educate their children and foster their health. We will bring peace and security, jobs and dignity to their lives. We will put more infrastructure at their service, to multiply their productivity and raise their incomes.

But this time things will be different. What wealth will be generated will be more equitably shared. What sacrifices are demanded will be more evenly carried. This much I promise, for every stone of sacrifice you carry, I will carry twice the weight.

This I promise the people. You will not be alone again in making sacrifices, and you will not be the last again to enjoy the rewards when they come.

I ask the rich to take a share of the sacrifices commensurate with their strength. What each of us carries is not our individual burden alone, but the fate of our country that we must all share, and which none of us can escape.

While I ask you to share these sacrifices with me, I will not impose any more on you when it comes to my job as president. The job is mine now and I�ll do it.

There is no excuse for the spread of crime in any society, unless government is an accomplice. There is no criminal organization or criminal activity that can stand up to the government if the government is sincere about stamping it out.

We know that the major crimes in this country are committed by hoodlums in uniform. We know they are protected by hoodlums in barong and acquitted by hood�lums in robes. We know that the most damaging crimes against society are not those of petty thieves in rags, but those of economic saboteurs in expensive clothes: the dishon�est stockholders, the wheeling dealing businessmen, influence-peddlers, price-padders and other crooks in government.

I promise to use all the powers of government to stamp out crime, big and small.

There will be no excuses and no exceptions. I sent friends to jail before; it was not my fault that the courts let them go.

No government is so powerless that it cannot protect its citizens, especially when they are victimized by government agents.

No government is so helpless, it cannot prosecute criminals, especially when the criminals are officials operating in the open.

And the government of a country, where most of the people are hungry, need jobs and lack education, cannot allow its taxes to be stolen or wasted, its assets thrown to friends, the national patrimony conceded to foreigners, and the best opportunities limited only to those who can afford.

There are things that a real government, even in the worst economic conditions, can do. This government will do it.

Government can stamp out crime, as I tried to do as chairman of the PACC, and as I will do as president of the Republic. This time nobody will clip my powers.

Government can provide basic services without the extra cost of pork barrel or kickback; roads for work; infrastructure for productivity; schools for skills; clinics for health; police for safety, and a lean and mean military machine for national defense. This I promise and I will deliver. I will give you at once a government that works, while we wait for the dividends of yet another round of sacrifice that must fall on your shoulders again.

Government cannot afford to feed all the hungry in our country, but it would be a crime if any money for food went to government officials and fixers instead.

Government cannot afford to build all the roads that are needed, but it would be a crime to build fewer roads to line more pockets.

Government cannot afford to bring back the millions of overseas Filipino workers to jobs and dignity back home, but we shall protect their interests abroad and their families back home.

Government cannot afford to give all the youth the complete education promised by the Constitution, but it would be a crime if any money for education was misspent on inferior textbooks and substandard classrooms built by pork barrel.

I appeal to the coming congress to search its conscience for a way to stand behind me, rather than against me, on the pork barrel issue and find a way to convert pork into tuition subsidies in the public and private schools.

These are crimes that I will make it my personal apostolate to punish:

�low crimes in the streets by rich or poor alike;

�high crimes on Ayala or Binondo;

�and graft and corruption throughout the government�executive, legislative and judicial.

This early, members of my family are swamped with offers of funny deals. I will treat all such offers as evidence for future criminal prosecutions for graft and corruption.

I warn these people. Going after criminals will just be a job for me, but if you drag in my family, it will be personal.

What I promise is not big. What I envision is ordinary. My promises are made to be fulfilled in a working day; they are hopes of ordinary Filipinos like myself, in circum�stances less than ideal with the economic recession, but they are long overdue.

I want to bring peace to our lives and harmony to our society. I want to bring order to our streets and justice to our institutions. I want to impart energy to our economy and more equitableness in the distribution of its fruits.

I want every Filipino, rich or poor, to feel that the safest place in the world for him is his own country.

And, lastly, I hope to bring all Filipinos together so as to achieve that power of common purpose that will enable us to escape the crisis of our region and achieve our centennial dream.

Freedom.

Freedom from oppression. Yes; but freedom from want also.

Freedom from fear and freedom of opportunity.

And, of course, freedom for its own sake which is the heart and soul of the Filipino.

It was here, one hundred years ago, that Asia sitting in darkness saw the first light of freedom.

Share my resolve to make that light shine brighter yet by making our freedom more real for the majority of the people.

For the past twelve years, the call has been for people power to defend democracy, advanced economic development and other things. It is time to use that power for the people themselves.

Now, power is with the people; one of their own has made it.


Official Gazette, http://www.gov.ph/1998/06/30/inaugural-address-of-president-estrada-official-english-translation-june-30-1998/