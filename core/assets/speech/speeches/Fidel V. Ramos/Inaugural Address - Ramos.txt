Sa loob ng nakalipas na siyamnapu at apat na taon, labing-isang pinunong Pilipino ang tumindig at naging bahagi ng ganitong seremonya ng ating demokrasya - na nagpa pahiwatig sa ating mahal na Republika ng makabuluhang pagpapatuloy at isang panibagong simula.

Ang kabanalan ng okasyong ito at ang panunumpa ng pangulo ng bansa ay isang paglingon sa nakaraan at pagharap sa kinabukasan.

Ang kagitingan at katapatan ni dating Pangulong Corazon C. Aquino ang naging dahilan ng muling pagkabuhay ng demokrasya at kalayaan sa ating bansa - at ito�y matagumpay na pinagtanggol laban sa mga rebelde ng ating lipunan.

Dahil kay dating Pangulong Aquino, ang demokrasya ay naging matatag na sandigan laban sa mga mapang api.


Continuity and a beginning


Over the last 94 years, 11 Filipino leaders before me have enacted this ceremony of democratic transition, which signifies for our Republic both continuity and a new beginning.

This consecration of the Presidency binds us to the past, just as it turns our hopes to the future.

My courageous predecessor, former President Corazon C. Aquino, restored our civil liberties - and then defended them tenaciously against repeated assaults from putschists and insurgents.

She has made our democracy a fortress against tyrants. Now we must use it to enable our people to take control of their lives, their livelihood and their future.

To this work of empowering the people, not only in their political rights but also in economic opportunities, I dedicate my Presidency.


The temper of the people


I see three elements in the stirring message of our people in the elections.

First, they spoke out against the old politics. They declared their resolve to be led along new paths and directions-toward the nation we long for-a nation peaceful, prosperous and just.

Second, they reaffirmed their adherence to the secular ideal-of Church and State separate but collaborating, coexistent but each supreme in its own domain. In this spirit, I see myself not as the first Protestant to become President, but as the twelfth Filipino President-who happens to be a Protestant and who must be President of Muslims, Christians and people of all faiths who constitute our national community.

Third, our people spoke of their faith that we Filipinos can be greater than the sum of all the problems that confront us, that we can climb higher than any summit we have already scaled.

We cannot but interpret the vote as a summons for us to unite and face the future together. The people are not looking for scapegoats, but for the basic things to get done-and get done quickly.

Let us begin by telling ourselves the truth. Our nation is in trouble. And there are no easy answers, no quick fixes for our basic ills. Once, we were the school of Southeast Asia. Today our neighbors have one by one passed us by.

What is to be done? There are no easy tasks, no soft comforts for those chosen by circumstances to forge from the crucible of crisis the national destiny.

We must make hard decisions. We shall have to resort to remedies close to surgery-to swift and decisive reform.

First, we must restore civic order. For without stability, businesses cannot run, workers cannot create wealth, liberty cannot flourish, and even individual life will be brutish and precarious.

Then, we must make politics serve-not the family, the faction or the party-but the nation.

And we must restructure the entire regime of regulation and control that rewards people who do not produce at the expense of those who do. A system that enables persons with political influence to extract wealth without effort from the economy.

The immediate future will be difficult in some areas. Things could get worse before they get better. Sacrifices will be asked of every sector of society. But I am not daunted, because crisis has a cleansing fire which makes heroes out of ordinary people and can transform a plodding society into a tiger.


Healing political wounds


Foremost among our concerns must be to bind the wounds of the election campaign and restore civility to political competition, for our people are weary of the intrigues and petty rivalries that have kept us down.

I will continue to reach out to all the groups and factions making up the political community. As early as possible, I will consult with the leaders of the Senate and the House of Representatives to work out the priorities of the legislative agenda.

I call on our mutinous soldiers and radical insurgents to give up their armed struggle. I will work with Congress in fashioning an amnesty policy that will enable errant reformists to re enter civil society.

When the time is opportune, I also intend to ask Congress to convene itself as a Constituent Assembly to amend the Constitution.�

Let us strive to make our political system fairer to all and more representative of the vastness and variety of our country. Let us all lay to rest our enmities and our conflicts, and this once join together in the reform and renewal of our society.

There are enough problems to engage us all; and if we surmount them, there will be enough glory to share.


Return to economic growth


Next in our priorities is to nurse the economy back to health and propel it to growth.

We must get the entire economy to generate productive employment   keeping in mind that for each citizen, a job means not merely material income, but social usefulness and self respect.

Here, too, we must begin with the basics-the social services that Government must provide, but has not; foundations of economic health, which we should have set up  long ago, but have not.

We cannot dream of development while our homes and factories are in darkness. Nor can we exhort enterprise to effort as long as Government stands as a brake-and not as a spur - to progress.

Both farm and factory must be empowered to produce more and better.

Deregulation and privatization shall set free our industries from the apron strings of the State.

Dismantling protectionist barriers and providing correct incentives and support shall make our industry more efficient and world-competitive-and our exports, the spearhead for economic revival and growth.

The last Congress has given us the law opening the economy to foreign investments. Our job now is to make that law come to life.

What we do for industry, we will supply in equal measure for agriculture, primarily because almost half of all our workers still live on it. And equally because agriculture is the foundation for our industrial modernization.

In this effort, we need a more realistic agrarian reform law which we can fully implement for the empowerment of our farmers. Keeping productivity and effective land use uppermost on our minds, let us set clear targets and do what is practicable.

Let us be firm about the paramount object of our labors. It is to uproot the poverty that grips our land and blights the lives of so many of our people.


A moral war on poverty


I have asked Mang Pandoy and his family to be my guests in this inaugural ceremony - as proof of my resolve to obtain for families like theirs all over the country the humanities of life. Poverty we must learn to regard as another form of tyranny, and we must  wage against it the moral equivalent of war.

In this work of expanding the life choices of the poorest among us, my Government will work hand in hand with non-government organizations and people�s organizations.

Throughout the campaign, I heard it said over and over that our national decline derives not from any flaw in the national character   or any failing of the individual Filipino-but from government�s historic failure to lead.

We cannot deny the logic of that verdict. For when the systems, rules and conditions are fair and sound, we Filipinos have excelled- sometimes to the astonishment of the world.

My administration will prove that government is not unavoidably corrupt-and that bureaucracy is not necessarily ineffective.

Graft and corruption we will confront more with action results than with words. We will go after both the bribe-takers and the bribe givers. The bigger the target, the greater will be the Government�s effort.

We will prove that effective and efficient government is possible in this country. Not just in national administration, but in the governing of our local communities.

The road to development is by now much traveled. We Filipinos have lacked not the way, but the will. This political will, my Presidency shall provide.


Our foreign relations


In foreign relations, we shall strive to strengthen ties with old friends and trading partners and we shall endeavor to develop new friendships.

My Government begins its term in a world transformed. The tide of freedom rising everywhere should help along our efforts to make democracy work here at home.

By the gift of Providence, our archipelago is strategically located in the critical sea lanes of Asia and the Pacific. This geopolitical fact shapes our relations with the world-a sense of responsibility for the building of peace and stability in our region, and a recognition of opportunity in our quest for development.

Diplomacy for development will be our central foreign policy thrust.

While residual political military dangers may linger in the region, securing continued access to markets and technology must become Southeast Asia�s primary concern. This we will pursue in concert with our regional partners and neighbors.

Can we accomplish all we need to do within six years? Yes, we can. We can lay the ground for self sustaining growth and more. But we can win the future only if we are united in purpose and in will.

The Filipino State has historically required extraordinarily little of its citizens. As individuals, we Filipinos acknowledge few obligations to the national community. Yet, if we are to develop, citizenship must begin to count more than ties of blood and kinship. Only with civic commitment does development become possible in a democratic society.


Private irresponsibility


Certainly, there can be no more tolerance of tax evasion, smuggling and organized crime-no matter how highly placed those who commit it. Nor can we continue to turn a blind eye to the social costs of unbridled profit.

The loss of our forests, the desiccation of our soil, the drying up of our watercourses, and the pollution of our cities�these are the public consequences of private irresponsibility. We must stop this profligate use and abuse of our natural resources, which are ours only in trust for those who will come after us.

Some of us think that empowerment means solely the access of every citizen to rights and opportunities. I believe there is more to this democratic idea. Our ideology of Christian democracy, no less than its Muslim counterpart, tells us that power must flow to our neighborhoods, our communities, our groups, our sectors and our institutions-for it is by collective action that we will realize the highest of our hopes and dreams.

During my term, we will be celebrating the centennial of our national revolution-those shining years between 1896 and 1898 when we were a beacon of freedom for the whole of colonial Asia.

Generations of our heroes-from Sultan Kudarat to Jos� Rizal-speak to us across history of the strength that unity can confer on any people.

Yet we Filipinos have always found unity difficult-even in the face of our crises of survival.

We were conquered by colonizers because we did not know our own strength.

Today, in the midst of our trials, we must learn how strong we can be- if only we stand together. This nation, which is the collective sum of our individual aspirations, cannot remain divided by distrust and suspicion. Either we rise together-above our self-centered bickerings and factional quarrels-or we fall into the pits we have dug for one another.

In 1890 Rizal, envisioning �The Philippines a Century Hence,� regarded as inevitable-as decreed by fate-the advancement and ethical progress of the Philippines.


Redemption is in our hands


We who are closer to that time have a more diminished sense of our possibilities.

Kung nais nating matupad ang pangarap ni Rizal - �Karagdagang katarungan at malawak na kalayaan �-sundin natin ang kanyang tagubilin:

Itakwil ang pagkawatak-watak � yakapin ang pagkakaisa � at minsan pa�y buhayin natin ang diwa ng ating bansa.

Tulad ng natanaw ni Rizal, ngayon na ang panahon upang sabihin sa ating sarili-na kung nais nating makaahon, kung nais nating umunlad, dapat tayo�y kumilos sa ating sariling pagsisikap. Sa pagkilos na ito, sabi ni Rizal, �dapat nating ibuhos ang buong liwanag ng ating mga kaisipan at lahat ng tibukin ng ating puso.�

Sa aking pagsisilbi sa bayan, ang aking pinakamatagal na serbisyo ay bilang kawal-Pilipino-kaya marahil ay kulang ako sa taginting ng isang orador kung ihahambing sa mga nauna sa akin sa panguluhan ng ating bansa.

Subali�t ako�y nakikiisa sa kanilang pananaw at pangarap. Ang bansang ito�y magtatagumpay. Ang bansang ito�y mananaig. Ang bansang ito ay uunlad muli-kung tayo ay magkakaisa.

Nasa harap natin ang pagsubok at paghamon; harapin natin nang sama sama at nagkakaisa.

Huwag tayong mawawalan ng pagtitiwala sapagka�t hindi tayo mabibigo. Ang ating mga layunin, ang ating mga mithiin, ay makatarungan, kaya ang Panginoon ng ating mga ninuno ay tikay na pumapatnubay sa atin.


Doing as Rizal prescribed


If we are to attain what Rizal wished for his posterity-�More law and greater liberty�-we must do as he prescribed. We must stifle our dissensions and summon once more the spirit of this nation.

As Rizal foresaw, the time has come to tell ourselves that if we wish to be saved, we must redeem ourselves. And in this work of self redemption, we must �expend the whole light of our intellect, and all the fervor of our hearts.�

For most of my public life, I have been mainly a citizen soldier, wanting in eloquence compared to those who have preceded me in this rite of democratic transition. But I share their vision of what our nation can become. This nation will endure, this nation will prevail and this nation will prosper again-if we hold together.

Before us lies the challenge: Come then, let us meet it together. With so much for us to do, let us not falter. With so little time left in our hands, we cannot afford to fail.

And with God�s blessing for all just causes, let us make common cause to win the future.


Source: Trinidad Fernandez, et. al., ed. Ours must be a Nation of Empowered People: The Inauguration of President Fidel V. Ramos(Makati City: Studio 5 Publishing, Inc. 1993),�-.

http://www.gov.ph/1992/06/30/inaugural-address-of-president-ramos-june-30-1992/
