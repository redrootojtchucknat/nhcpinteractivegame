Philippine President Manuel L. Quezon with his wife, First Lady Aurora Aragon Quezon together with their children, kneel as they received Holy Communion during the Holy Sacrifice of the Mass.

"It is, therefore, the keen desire of the Church that all of the faithful kneel at the feet of the Redeemer to tell Him how much they venerate and love Him." -Pope Pius XII, Mediator Dei