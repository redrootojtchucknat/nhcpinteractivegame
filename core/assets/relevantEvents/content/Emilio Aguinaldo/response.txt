Aguinaldo's response to General Otis' proclamation:

"The proclamation of Senor E. S. Otis, Major General of the United States Volunteers, published in the papers of Manila, was presented to me. I do most solemnly protest against all of the document and all the sentiments it contains. I protest with all my soul, conscience and strength against the unjust authority, which Major General Otis, as Military Governor, has assumed, in relation to our politics.

General Otis, referring to his proclamation, has assumed the right to direct affairs, and against that authority I energetically protest.

I do most solemnly affirm that in Singapore, Hong-kong or here I did not enter into any compromising agreement with the Americans.

On the contrary, on the nineteenth of May, previous to the hostilities with Spain, I was transported to an American ship of war and there assured that the Americans did not desire the conquest of the Filipines, but would guarantee us our liberty and independence. This I announced in my official proclamation of the twenty-fourth of May. On June 12th, when we raised for the first time, in my native city of Cavite, our sacred flag, the national emblem of noble inspirations, I told my people of the words of General Merrit, in a manifesto, had stated, days before the Spanish General Jaudenes had surrendered. He also assured me that the Filipine forces would have possession of the city of Manila. General Otis has contradicted every promise given me, for they have not been carried out.

Furthermore, the United States has recognized by its army and navy, the beligerency of the Filipine forces. It is evident that primarily the United States did not contemplate the annexation of this territory, for its sole representative here was the same as other nations, i. e., consular representation.

In his proclamation, General Otis refers to the instructions deduced from the message of the president of the United States, which pertained to the government of the islands. I do solemnly protest against these instructions in the name of God; in the name of reason and in the name of justice. That all my brothers shall be protected, my conscience forces me to further protest against the intrusion of the United States in the Filipines, and I equally protest in the name of all my people against the policy of the United States. The people of the Filipines have given me a vote of confidence and I will struggle for liberty and independence until death.

And as a last protest against the encroachments of the Americans I call up what has transpired in the past. My relations with the American authorities, and what I was given to understand in Hong-kong to bear witness in our behalf. I was given to understand that the war with Spain would redound to the benefit of our liberty. Such was our verbal agreement.

And now, my dear brothers, you understand fully, all which passed in regard to the declarations of America, and now it devolves upon us as our prime duty to struggle with all strength for absolute independence and fullfilment of our noble aspirations. The strength of convictions should carry us forward, until when we return our pathway shall be that of glory.

EMILIO AGUINALDO.

Malolos, Jan 5, 1899"