At 2:00 a.m. of  March 28, the USS Vicksburg anchored in Manila Bay, with all lights screened, to keep the return of the expedition secret.

At 6:00 a.m., General Funston and Aguinaldo, accompanied by some officers, boarded one of the launches and left the USS Vicksburg.

They went up the Pasig River to the residence of the Governor-General in Malaca�an, where they disembarked. Aguinaldo was presented to Brig. Gen. Arthur C. MacArthur, Jr. as a prisoner of war. He was treated by the Americans more as a guest than as
a prisoner.

(Taken from http://philippineamericanwar.webs.com/captureofaguinaldo1901.htm)