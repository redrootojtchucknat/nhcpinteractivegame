At breakfast, MacArthur promised Aguinaldo that he would immediately send for his family, whom he had not seen for a long time.
 
Aguinaldo complimented his captors: "At all times since our capture, as well in Palanan as on board the Vicksburg, we have been treated with the highest consideration by our captors, as well as by all the other American officers with whom we have come in contact."

(Taken from http://philippineamericanwar.webs.com/captureofaguinaldo1901.htm)