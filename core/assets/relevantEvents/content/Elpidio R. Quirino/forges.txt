MANILA - (AP) - President Elpidio Quirino, speaking on the third anniversary of Philippine independence, again called for a Pacific union as "our answer to the threat of Red imperialism."

He addressed a acrown estimated at 80,000 persons gathered to witness a three-hour military and civic parade.

Quirino said that communism was in retreat in the West but at flood tide in the East.

"We can not afford to think that we or any other free nation here can remain free if the countries around us fail to conserve their gains toward freedom until they fully attain it," he said.

"Our answer to the threat of Red imperialism and new slavery is a real union of peoples around the Pacific on the basis of common counsel and assistance," Quirino continued. "To this our republic feels bound to commit itself if it is to continue the pace of its progress and see its neighbors share the blessings of freedom and peace under a democratic system."

The president said the Philippine government was born in an atmosphere of crisis and want, "but now it is hitting its stride."

He predicted the islands' political disturbance would disappear shortly after the presidential election on Nov. 8.

(Taken from http://news.google.com/newspapers?nid=950&dat=19490705&id=xwNQAAAAIBAJ&sjid=hFUDAAAAIBAJ&pg=4336,5526286)