MANILA - President Elpidio Quirino proposed Saturday that the United States bolster far eastern democracies in their battle against communism by taking the lead in formation of a Southeast Asian economic union.

Quirino, defeated in his bid for re election this month, outlined his plan to U.S. Vice President Richard Nixon during a 1-hour, 15-minute conference - the longest Nixon has had with a chief of state during his good-will tour of the Pacific.

There had been reports Quirino would renew his angry charges of U.S. intervention in the Philippines presidential; election. But there was no mention of politics at the meeting.

Quirino said a Southeast Asian union should be economic rather than military because Pacific nations have insufficient military strength. He expressed confidence that the cold war against communism will be won with U.S. aid.

The Philippines president-elect Ramon Magsaysay, said in an interview Saturday that Quirino has expected all Southeast Asian nations to accept him as a leader whereas first "we must win their confidence."

Magsaysay, who talked with Nixon on last night, implied that he favors Filipino leadership of a Southeast Asia alliance, but intends to proceed slowly.

The president-elect declinbed to say what he and Nixon talked about.

"We just sat and stared at each other," he said with a grin. Actually the two had an animated conversation for nearly an hour.

Magsaysay said the problem of Communist-led Huk outlaws in the Philippines no longer is serious. He said there are only about 3,000 Huks left and once he takes office Dec. 31. "I'll go after them and finish them."

(Taken from: http://news.google.com/newspapers?nid=1817&dat=19531120&id=IRcfAAAAIBAJ&sjid=NZgEAAAAIBAJ&pg=7266,2298414)