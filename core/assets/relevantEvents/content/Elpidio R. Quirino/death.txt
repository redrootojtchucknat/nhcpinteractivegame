Manila, P. I. - Former President Elpidio Quirino died Wednesday at his home at the age of 65. Quirino assumed the presidency on the death of President Manuel Roxas in 1948 when the republic was only 21 months old. He lost to President Ramon Magsaysay in the 1953 election. Quirino had long suffered from heart trouble. He was recovering from an attack when he rose from vice-president to president in 1948. When he took the oath of office he said, "I am living on borrowed time."

During World War II, the Japanese made efforts to lure Quirino into the puppet government of the Philippines. He refused and was imprisoned for 45 days.

Just before the war's end, his wife and three of their five children were massacred by the Japanese, but Quirino and the other two children made their way to the liberating United States forces.

(Taken from http://news.google.com/newspapers?nid=1499&dat=19560229&id=g1kaAAAAIBAJ&sjid=ZyUEAAAAIBAJ&pg=7210,3850228)