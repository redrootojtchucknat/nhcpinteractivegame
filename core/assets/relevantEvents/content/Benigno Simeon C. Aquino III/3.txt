In the Philippines, the Liberal Party's presidential candidate Benigno Simeon Cojuangco Aquino III, commonly known as Noynoy, was officially elected as the next President of the Philippines in the 2010 presidential election. The Commission of Elections announced Noynoy's victory on Wednesday.

He campaigned for an end to corruption and poverty and says he'll make prosecuting corrupt officials a priority. Noynoy received more than 15 million votes, about 5.7 million ahead of his closest opponent, ousted Joseph Estrada.

Noynoy said in an interview, "I want to lead by example. We talk about corruption. I did make a public vow, I will never steal". Senate President Juan Ponce Enrile told a crowd after the count: "We have done this ... for the Filipino people ... This is a historic event."

He is 50 years old, a bachelor, an economics graduate, a Senator and is the only son of former President Corazon Aquino and former Senator Benigno Aquino, Jr.. Noynoy's father was shot while in military custody during the regime of late dictator Ferdinand Marcos, before his mother, Corazon Aquino, led the People Power Revolution that toppled Marcos in 1986. She then served as president for six years.

In the Philippines, the president and vice president are elected separately; the Puwersa ng Masang Pilipino Party's vice-presidential candidate Jejomar Cabauatan Binay will become the country's vice-president. Noynoy is the President-elect, while Gloria Macapagal-Arroyo is the outgoing incumbent. He will officially become the Philippines's fifteenth president on June 30.

(Taken from http://en.wikinews.org/wiki/Noynoy_Aquino_elected_Philippine_president)