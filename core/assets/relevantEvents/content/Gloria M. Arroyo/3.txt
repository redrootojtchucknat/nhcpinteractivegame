Unfazed by an impeachment complaint, Philippine Vice-President Gloria Macapagal-Arroyo is drawing up an alternative agenda for the Philippines.

The daughter of former president Diosdado Macapagal wants to ensure there is no vacuum if and when President Joseph Estrada resigns or is removed from office.

"I am prepared to take over," she declares at her posh vice-presidential office in the Philippine International Convention Centre in Metro Manila.

The former classmate of US President Bill Clinton is pretty. She looks like a movie star and appears very much younger than her 53 years. Macapagal readily admits that she stands to gain the most if Estrada quits or is removed from office.

The masters in economics graduate is being called an opportunist and some even think her arrogant for preparing to take over when Estrada's impeachment trial has only just started. Her critics say she is "lusting" for the presidency.

Macapagal says allegations of "large-scale corruption" against Estrada forced her to quit his Cabinet as Social Welfare and Development Secretary and join the opposition call for his resignation.

"There has to be a line drawn somewhere. I had no choice but to quit. My decision to quit was also influenced by (Manila Archbishop) Cardinal Sin's call to Estrada to resign."

Saying she is against any form of corruption, Macapagal stresses that leadership must be by example.

"If a president is corrupt, then it will be difficult for people down below to resist the temptation."

Although, somewhat small in size, she easily assumes a commanding posture and gives the impression of someone who will be able to get things done.

She says several groups are working with her to draw up an alternative agenda with four components.

"The first pertains to an economic philosophy of free enterprise. We need a national goal to drive us. I would like the President to be bold in implementing our national ambition."

"And I would aim to win the battle against poverty within 10 years. There has to be a social and sectoral bias in our economic development plan. While aspiring for a world class internet capability is a matter of future imperative, today's main battle is still to eliminate the inequity and poverty within our society."

She says structural reforms would be undertaken and safety nets put in place to protect those who would be affected by globalisation. The agriculture sector would be modernised and land reforms would be instituted, she adds.

Macapagal says she would push for higher standards in morality both in government and society.

She says she is against the politics of patronage and hopes to wean the country away from this.

"I am one who feels strongly about leadership by example and the importance of performance over grandstanding."

She says in drawing up the alternative agenda she has consulted various groups, including academicians and leaders of civil society.

On the State's involvement in gambling, she says: "The Government should not be involved. In the long run the State under me will divorce itself from this but we have first to study and fulfill existing contractual obligations. We certainly won't enter into new gambling contracts."

She says the country is suffering. The economy is doing badly, investors have lost confidence and the people are unhappy with the President's ways.

"I think I can bring back the confidence of investors."

On Malaysia-Philippine relations, she says more should be done to strengthen relations. She would do this if she becomes President.

"We have a big Filipino population in Sabah, for instance, and there are many irritants that need to be resolved. We should have a Philippine diplomatic presence in Sabah to settle these irritants. It will help make things smoother."

"Malaysia has a prosperous economy and we will urge businessmen to further improve their contracts with Malaysian businessmen. We will invite more Malaysian businessmen to the Philippines."

Macapagal hopes that the Malaysia-Indonesia-Brunei-Philippines East Asian Growth triangle project can be revived so that "we can have join projects in the economic area."

Regarding the Philippine's claim on Sabah, Macapagal says: "It can stay in the back burner. The claim does not have to affect our relationship."

She does not expect to face any problems from the civil service, the police or the military.

"By and large, the civil service, the police and the military are loyal to whoever is president. Civil servants have gone through so many crises and problems. They are non-political."

"The military, especially now, more than a decade after martial law under Marcos (former president Ferdinand Marcos who was ousted in a popular people's power movement in 1986), is faithful to the Constitution. The police feel that way too."

But her alternative agenda hinges on whether she gets impeached or not. For Marcos loyalist Oliver Lozano has lodged an impeachment complaint against her with the House of Representatives and she has sent in her reply.

Lozano has charged Macapagal with culpably violating the Constitution and betraying public trust by joining the call for Estrada to resign and leading opposition rallies.

A one-third vote is enough to send the matter to the Senate for an impeachment hearing and Estrada's ruling coalition has enough support to do this. Which is why she sighed in relief when the House postponed discussion on the matter pending the outcome of Estrada's impeachment trial.

Macapagal also has another case, filed in 1998 with a Senate committee, the she and her husband, lawyer Miguel Arroyo, own multi-million peso condominiums in San Francisco, US, but did not reveal this in her financial statements as required by law.

But Macapagal is taking all this in her stride, including allegations that she has links to a man considered to be a gambling lord who hails from her hometown of Lubao, Pampanga. She feels these are simply attempts by her political enemies to tarnish her image.

The support of the people is paramount for any politician. And Macapagal is confident of the backing of the majority of Filipinos.

Macapagal received the highest number of votes, almost 16 million, when she was re-elected Senator in 1995. She had a majority of more than seven million votes when she was elected vice-president in 1998. Public opinion polls, which are frequently undertaken in the Philippines, have showed her ministry getting top ratings.

Macapagal and Arroyo have three children - eldest Miguel Arroyo Jr., 30, is an actor who will be running for the post of vice-governor of Pampanga in the May elections, Maria Lourdes Evangeline Arroyo, 29, is married and Diosdado Macapagal Jr., 27, is studying law at the Ateneo de Manila University.

She has been successful in running her family and her carrer. Now, she is relishing the prospect of running her country.

(Taken from New Straits Times - Jan 15, 2001)