Vice President Gloria Macapagal Arroyo said yesterday that her brother-in-law had already sold two pieces of real state property in San Francisco in the United States, which her detractors claimed were really owned by her and her husband.

Macapagal made the disclosure at the television show of movie actress Kris Aquino, where she was asked about the US properties of her brother-in-law Ignacio Arroyo, which were valued at P120 million when they were purchased in 1992.

"He already sold it. It's academic," Macapagal said of the property, which consisted of a 35-unit apartment building at 737 Bush St. and a commercial condominium at 1776 Sacramento corner Van Ness Avenue sitting on a 1,000-square meter lot.

Arroyo himself confirmed that he sold the Van Ness condominium on Sept. 11, 1998 and the Bush property on June 14, 1999.

Arroyo apparently did not make much money from selling his seven-year-old investments.

The Van Ness property, consisting of two units acquired by Arroyo acquired for $2.55 million, was sold for $3.1 million. The property was sold to William and Rosemarie Garlock of the Garlock Family Trust.

The Bush property was acquired by the Prana Capital, a US-based corporation, for $2.4 million. It was originally bought for $2.05 million.

Asked by the Inquirer why he decided to sell the property, Arroyo said he was angered by the way they were being used against his sister-in-law.

"I'm not used to this kind of attention," Arroyo also said of the controversy, which hounded him, after some quarters alleged that Macapagal and her husband were the true owners of the property.

Arroyo has always contended that he bought the property from the inheritance he got from his late mother, Lourdes Arroyo, who belonged to the landed Tuasons.

In 1998, Macapagal, who was at the time a senator, was charged with graft before the Office of the Ombudsman by lawyer Ricardo Valmonte.

Valmonte alleged that Macapagal did not declare the US property acquisitions in her statement of assets and liabilities in 1996 and 1997.

Macapagal was cleared by the Ombudsman in June 1998.

Macapagal's alleged ownership of this property came up again in November last year when it was reported that her brother-in-law's income tax returns showed he had not the means to pay for the property.

The Vice President reiterated yesterday the revival of the "old charge" was part of the black propaganda being waged against her by her detractors.

She also said that according to her brother-in-law, it was against the law to release income tax returns.

"Only the President can authorize anybody to publish income tax returns," she said.

(Taken from Philippine Daily Inquirer - Jan 3, 2001)